CREATE TYPE rankby AS ENUM (
    'time',
    'views',
    'votes',
    'comments',
    'votesperviews',
    'commentsperviews'
);

CREATE TABLE users(
  id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  name text UNIQUE,
  email text UNIQUE,
  password_hash text,
  created_at timestamptz NOT NULL DEFAULT NOW(),
  last_activity_at timestamptz NOT NULL DEFAULT NOW(),
  banned boolean NOT NULL DEFAULT FALSE,

  artilce_rank_by rankby,
  article_time_period interval,
  article_time_start timestamptz,

  article_last_viewed_period interval,
  article_filter_viewed boolean,
  article_filter_reported_users boolean,

  article_filter_hidden_topics boolean,
  article_filter_hidden_users boolean,
  article_filter_hidden_articles boolean,
  article_filter_anonymous boolean,
  article_filter_adult boolean,

  article_buffer_format_id int REFERENCES formats(id),
  article_buffer_format_content text,

  comment_rank_by int,
  comment_last_viewed_period interval,
  comment_filter_viewed bolean,
  comment_filter_reported_users boolean,

  comment_filter_hidden_users boolean,
  comment_filter_hidden_comments boolean,
  comment_filter_anonymous boolean,
  comment_filter_adult boolean,

  comment_buffer_format_id int REFERENCES formats(id),
  comment_buffer_format_content text,
);

CREATE TABLE article_user_report_thresholds(
  user_id bigint REFERENCES users(id),
  report_type_id int REFERENCES report_types(id),
  threshold real NOT NULL,
  PRIMARY KEY (user_id, report_type_id)
);

CREATE TABLE comment_user_report_thresholds(
  user_id bigint REFERENCES users(id),
  report_type_id int REFERENCES report_types(id),
  threshold real NOT NULL,
  PRIMARY KEY (user_id, report_type_id)
);

CREATE TABLE formats(
  id smallint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  name text NOT NULL,
  command text NOT NULL
);

CREATE TABLE articles(
  id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id bigint REFERENCES users(id) NOT NULL,
  format_id int REFERENCES formats(id),
  title text NOT NULL,
  html text NOT NULL,
  source text,
  created_at timestamptz NOT NULL,
  anonymous boolean NOT NULL DEFAULT FALSE,
  adult boolean NOT NULL DEFAULT FALSE,
  deleted boolean NOT NULL DEFAULT FALSE,
  CHECK ((format_id IS NULL AND source IS NULL) OR (format_id IS NOT NULL AND source IS NOT NULL))
);

CREATE TABLE article_edits(
  id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  article_id bigint REFERENCES articles(id),
  title_patch text,
  html_patch text,
  source_patch text,
  edited_at timestamptz NOT NULL,
  CHECK (title_patch IS NOT NULL OR html_patch IS NOT NULL)
);

CREATE INDEX article_edits_article_id_idx ON article_edits(article_id);

CREATE TABLE comments(
  id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  user_id bigint REFERENCES users(id) NOT NULL,
  article_id bigint REFERENCES articles(id) NOT NULL,
  parent_comment_id REFERENCES comments(id),
  format_id int REFERENCES formats(id),
  html text NOT NULL,
  source text,
  created_at timestamptz NOT NULL,
  anonymous boolean NOT NULL DEFAULT FALSE,
  adult boolean NOT NULL DEFAULT FALSE,
  deleted boolean NOT NULL DEFAULT FALSE,
  CHECK ((format_id IS NULL AND source IS NULL) OR (format_id IS NOT NULL AND source IS NOT NULL))
);

CREATE TABLE comment_edits(
  id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  comment_id bigint REFERENCES articles(id),
  html_patch text NOT NULL,
  source_patch text,
  edited_at timestamptz NOT NULL,
);

CREATE INDEX comments_edits_comment_id_idx ON comment_edits(comment_id);

CREATE TABLE article_votes(
  user_id bigint REFERENCES users(id),
  article_id bigint REFERENCES articles(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, article_id)
);

CREATE TABLE comment_votes(
  user_id bigint REFERENCES users(id),
  comment_id bigint REFERENCES comments(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, comment_id)
);

CREATE TABLE article_views(
  user_id bigint REFERENCES users(id),
  article_id bigint REFERENCES articles(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, article_id)
);

CREATE TABLE comment_views(
  user_id bigint REFERENCES users(id),
  comment_id bigint REFERENCES comments(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, comment_id)
);

CREATE TABLE topics(
  id bigint PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  name text UNIQUE NOT NULL,
  summary text NOT NULL,
  wikipedia_link text
);

CREATE TABLE topic_edges(
  prev_topic_id REFERENCES comments(id) NOT NULL,
  next_topic_id REFERENCES comments(id) NOT NULL,
  PRIMARY KEY (prev_topic_id, next_topic_id)
);

CREATE TABLE articles_topics(
  article_id bigint REFERENCES articles(id),
  topic_id bigint REFERENCES topics(id),
  PRIMARY KEY (article_id, topic_id)
);

CREATE TABLE users_topics(
  user_id bigint REFERENCES users(id),
  topic_id bigint REFERENCES topics(id),
  PRIMARY KEY (user_id, topic_id)
);

CREATE TABLE hidden_users(
  user_id bigint REFERENCES users(id),
  hidden_user_id bigint REFERENCES users(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, hidden_user_id)
);

CREATE TABLE hidden_topics(
  user_id bigint REFERENCES users(id),
  hidden_topic_id bigint REFERENCES topics(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, hidden_topic_id)
);

CREATE TABLE hidden_articles(
  user_id bigint REFERENCES users(id),
  hidden_article_id bigint REFERENCES articles(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, hidden_article_id)
);

CREATE TABLE hidden_comments(
  user_id bigint REFERENCES users(id),
  hidden_comment_id bigint REFERENCES comments(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, hidden_comment_id)
);

CREATE TABLE saved_articles(
  user_id bigint REFERENCES users(id),
  article_id bigint REFERENCES articles(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, article_id)
);

CREATE TABLE saved_comments(
  user_id bigint REFERENCES users(id),
  comment_id bigint REFERENCES comments(id),
  time_stamp timestamptz NOT NULL,
  PRIMARY KEY (user_id, comment_id)
);

CREATE TABLE report_types(
  id int PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  name text NOT NULL,
  description text NOT NULL
);

CREATE TABLE users_views(
  user_id bigint REFERENCES users(id),
  viewed_user_id bigint REFERENCES users(id),
  PRIMARY KEY (user_id, viewed_user_id)
);

CREATE TABLE user_reports(
  user_id bigint REFERENCES users(id),
  reported_user_id bigint REFERENCES users(id),
  report_type_id int REFERENCES report_types(id),
  PRIMARY KEY (user_id, reported_user_id, report_type_id)
);

