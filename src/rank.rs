use self::{
    article::{ArticleRankerTimes, ArticleScoreRanker, ArticleTimeRanker},
    comment::CommentRanker,
};
use crate::{
    db::getter::{
        DatabaseGetSublistRankerTimes, DatabaseGetSublistScoreRanker, DatabaseGetSublistTimeRanker,
        DatabaseGetTreeRanker,
    },
    filter::{ArticleFilterReadLock, CommentFilterReadLock},
    Id, Time,
};
use ordered_float::NotNan;
use serde::{Deserialize, Serialize};
use sqlx::Error;

mod article;
mod comment;
pub mod sublist;
pub mod tree;

#[derive(Serialize, Deserialize, Debug, Clone, Copy, sqlx::Type)]
#[sqlx(type_name = "rankby", rename_all = "lowercase")]
pub enum RankBy {
    Time,
    Views,
    Votes,
    Comments,
    VotesPerViews,
    CommentsPerViews,
}

pub struct ArticleRankerHandler<
    D1: DatabaseGetSublistTimeRanker,
    D2: DatabaseGetSublistScoreRanker<i64>,
    D3: DatabaseGetSublistScoreRanker<i64>,
    D4: DatabaseGetSublistScoreRanker<i64>,
    D5: DatabaseGetSublistScoreRanker<NotNan<f64>>,
    D6: DatabaseGetSublistScoreRanker<NotNan<f64>>,
    D7: DatabaseGetSublistRankerTimes + Clone,
> {
    article_time_ranker: ArticleTimeRanker<D1, D7>,
    article_views_ranker: ArticleScoreRanker<i64, D2, D7>,
    article_votes_ranker: ArticleScoreRanker<i64, D3, D7>,
    article_comments_ranker: ArticleScoreRanker<i64, D4, D7>,
    article_votes_per_views_ranker: ArticleScoreRanker<NotNan<f64>, D5, D7>,
    article_comments_per_views_ranker: ArticleScoreRanker<NotNan<f64>, D6, D7>,
}

impl<
        D1: DatabaseGetSublistTimeRanker,
        D2: DatabaseGetSublistScoreRanker<i64>,
        D3: DatabaseGetSublistScoreRanker<i64>,
        D4: DatabaseGetSublistScoreRanker<i64>,
        D5: DatabaseGetSublistScoreRanker<NotNan<f64>>,
        D6: DatabaseGetSublistScoreRanker<NotNan<f64>>,
        D7: DatabaseGetSublistRankerTimes + Clone,
    > ArticleRankerHandler<D1, D2, D3, D4, D5, D6, D7>
{
    pub fn new(
        db_time_ranker: D1,
        db_views_ranker: D2,
        db_votes_ranker: D3,
        db_comments_ranker: D4,
        db_votes_per_views_ranker: D5,
        db_comments_per_views_ranker: D6,
        db_ranker_times: D7,
        cutoff: usize,
    ) -> ArticleRankerHandler<D1, D2, D3, D4, D5, D6, D7> {
        let times = ArticleRankerTimes::new(db_ranker_times);
        ArticleRankerHandler {
            article_time_ranker: ArticleTimeRanker::new(db_time_ranker, times.clone(), cutoff),
            article_views_ranker: ArticleScoreRanker::new(db_views_ranker, times.clone(), cutoff),
            article_votes_ranker: ArticleScoreRanker::new(db_votes_ranker, times.clone(), cutoff),
            article_comments_ranker: ArticleScoreRanker::new(
                db_comments_ranker,
                times.clone(),
                cutoff,
            ),
            article_votes_per_views_ranker: ArticleScoreRanker::new(
                db_votes_per_views_ranker,
                times.clone(),
                cutoff,
            ),
            article_comments_per_views_ranker: ArticleScoreRanker::new(
                db_comments_per_views_ranker,
                times,
                cutoff,
            ),
        }
    }

    pub async fn get(
        &self,
        rank_by: RankBy,
        topic_ids: &[Id],
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        match rank_by {
            RankBy::Time => {
                self.article_time_ranker
                    .get(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Views => {
                self.article_views_ranker
                    .get(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Votes => {
                self.article_votes_ranker
                    .get(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Comments => {
                self.article_comments_ranker
                    .get(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::VotesPerViews => {
                self.article_votes_per_views_ranker
                    .get(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::CommentsPerViews => {
                self.article_comments_per_views_ranker
                    .get(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
        }
    }

    pub async fn get_rev(
        &self,
        rank_by: RankBy,
        topic_ids: &[Id],
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        match rank_by {
            RankBy::Time => {
                self.article_time_ranker
                    .get_rev(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Views => {
                self.article_views_ranker
                    .get_rev(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Votes => {
                self.article_votes_ranker
                    .get_rev(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Comments => {
                self.article_comments_ranker
                    .get(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::VotesPerViews => {
                self.article_votes_per_views_ranker
                    .get_rev(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::CommentsPerViews => {
                self.article_comments_per_views_ranker
                    .get_rev(topic_ids, start_time, end_time, amount, filter_lock)
                    .await
            }
        }
    }

    pub async fn single_get(
        &self,
        rank_by: RankBy,
        topic_id: Id,
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        match rank_by {
            RankBy::Time => {
                self.article_time_ranker
                    .single_get(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Views => {
                self.article_views_ranker
                    .single_get(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Votes => {
                self.article_votes_ranker
                    .single_get(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Comments => {
                self.article_comments_ranker
                    .single_get(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::VotesPerViews => {
                self.article_votes_per_views_ranker
                    .single_get(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::CommentsPerViews => {
                self.article_comments_per_views_ranker
                    .single_get(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
        }
    }

    pub async fn single_get_rev(
        &self,
        rank_by: RankBy,
        topic_id: Id,
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        match rank_by {
            RankBy::Time => {
                self.article_time_ranker
                    .single_get_rev(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Views => {
                self.article_views_ranker
                    .single_get_rev(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Votes => {
                self.article_votes_ranker
                    .single_get_rev(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::Comments => {
                self.article_comments_ranker
                    .single_get_rev(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::VotesPerViews => {
                self.article_votes_per_views_ranker
                    .single_get_rev(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
            RankBy::CommentsPerViews => {
                self.article_comments_per_views_ranker
                    .single_get_rev(topic_id, start_time, end_time, amount, filter_lock)
                    .await
            }
        }
    }
}

pub struct CommentRankerHandler<
    D1: DatabaseGetTreeRanker<()>,
    D2: DatabaseGetTreeRanker<i64>,
    D3: DatabaseGetTreeRanker<i64>,
    D4: DatabaseGetTreeRanker<i64>,
    D5: DatabaseGetTreeRanker<NotNan<f64>>,
    D6: DatabaseGetTreeRanker<NotNan<f64>>,
> {
    comment_time_ranker: CommentRanker<(), D1>,
    comment_views_ranker: CommentRanker<i64, D2>,
    comment_votes_ranker: CommentRanker<i64, D3>,
    comment_comments_ranker: CommentRanker<i64, D4>,
    comment_votes_per_views_ranker: CommentRanker<NotNan<f64>, D5>,
    comment_comments_per_views_ranker: CommentRanker<NotNan<f64>, D6>,
}

impl<
        D1: DatabaseGetTreeRanker<()>,
        D2: DatabaseGetTreeRanker<i64>,
        D3: DatabaseGetTreeRanker<i64>,
        D4: DatabaseGetTreeRanker<i64>,
        D5: DatabaseGetTreeRanker<NotNan<f64>>,
        D6: DatabaseGetTreeRanker<NotNan<f64>>,
    > CommentRankerHandler<D1, D2, D3, D4, D5, D6>
{
    pub fn new(
        db_time_ranker: D1,
        db_views_ranker: D2,
        db_votes_ranker: D3,
        db_comments_ranker: D4,
        db_votes_per_views_ranker: D5,
        db_comments_per_views_ranker: D6,
        cutoff: usize,
    ) -> CommentRankerHandler<D1, D2, D3, D4, D5, D6> {
        CommentRankerHandler {
            comment_time_ranker: CommentRanker::new(db_time_ranker, cutoff),
            comment_views_ranker: CommentRanker::new(db_views_ranker, cutoff),
            comment_votes_ranker: CommentRanker::new(db_votes_ranker, cutoff),
            comment_comments_ranker: CommentRanker::new(db_comments_ranker, cutoff),
            comment_votes_per_views_ranker: CommentRanker::new(db_votes_per_views_ranker, cutoff),
            comment_comments_per_views_ranker: CommentRanker::new(
                db_comments_per_views_ranker,
                cutoff,
            ),
        }
    }

    pub async fn get(
        &self,
        rank_by: RankBy,
        article_id: Id,
        parent_comment_id: Option<Id>,
        child_comment_ids: &[Id],
        amount: usize,
        filter_lock: CommentFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        match rank_by {
            RankBy::Time => {
                self.comment_time_ranker
                    .get(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::Views => {
                self.comment_views_ranker
                    .get(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::Votes => {
                self.comment_votes_ranker
                    .get(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::Comments => {
                self.comment_comments_ranker
                    .get(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::VotesPerViews => {
                self.comment_votes_per_views_ranker
                    .get(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::CommentsPerViews => {
                self.comment_comments_per_views_ranker
                    .get(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
        }
    }

    pub async fn get_rev(
        &self,
        rank_by: RankBy,
        article_id: Id,
        parent_comment_id: Option<Id>,
        child_comment_ids: &[Id],
        amount: usize,
        filter_lock: CommentFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        match rank_by {
            RankBy::Time => {
                self.comment_time_ranker
                    .get_rev(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::Views => {
                self.comment_views_ranker
                    .get_rev(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::Votes => {
                self.comment_votes_ranker
                    .get_rev(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::Comments => {
                self.comment_comments_ranker
                    .get_rev(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::VotesPerViews => {
                self.comment_votes_per_views_ranker
                    .get_rev(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
            RankBy::CommentsPerViews => {
                self.comment_comments_per_views_ranker
                    .get_rev(
                        article_id,
                        parent_comment_id,
                        child_comment_ids,
                        amount,
                        filter_lock,
                    )
                    .await
            }
        }
    }
}
