use crate::{
    db::{
        getter::{
            DatabaseGetArticleFlags, DatabaseGetCommentFlags, DatabaseGetIdSet,
            DatabaseGetUserFlags, DatabaseGetUserHiddenArticles, DatabaseGetUserHiddenComments,
            DatabaseGetUserHiddenTopics, DatabaseGetUserHiddenUsers,
            DatabaseGetUserReportThresholds, DatabaseGetUserViewedArticles,
            DatabaseGetUserViewedComments,
        },
        Database,
    },
    lower_bound_wilson_95ci, Arcrw, Id,
};
use parking_lot::{ArcRwLockReadGuard, RawRwLock, RwLock, RwLockReadGuard};
use sqlx::Error;
use std::{collections::HashSet, sync::Arc};
use tokio::try_join;

pub struct FilterHandler {
    default_report_thresholds: Option<RwLock<Vec<f64>>>,
    default_hidden_topics: Option<Arcrw<HashSet<Id>>>,
    default_hidden_users: Option<Arcrw<HashSet<Id>>>,
    default_hidden_articles: Option<Arcrw<HashSet<Id>>>,
    default_hidden_comments: Option<Arcrw<HashSet<Id>>>,

    default_filter_anonymous: bool,
    default_filter_adult: bool,
    default_filter_viewed: bool,

    db_article_flags: DatabaseGetArticleFlags,
    db_comment_flags: DatabaseGetCommentFlags,
    db_user_flags: DatabaseGetUserFlags,

    db_user_reports_thresholds: DatabaseGetUserReportThresholds,
    db_user_viewed_articles: DatabaseGetUserViewedArticles,
    db_user_viewed_comments: DatabaseGetUserViewedComments,

    db_user_hidden_topics: DatabaseGetUserHiddenTopics,
    db_user_hidden_users: DatabaseGetUserHiddenUsers,
    db_user_hidden_articles: DatabaseGetUserHiddenArticles,
    db_user_hidden_comments: DatabaseGetUserHiddenComments,
}

impl FilterHandler {
    pub fn new(
        db: Arc<Database>,
        default_report_thresholds: Option<RwLock<Vec<f64>>>,
        default_hidden_topics: Option<Arcrw<HashSet<Id>>>,
        default_hidden_users: Option<Arcrw<HashSet<Id>>>,
        default_hidden_articles: Option<Arcrw<HashSet<Id>>>,
        default_hidden_comments: Option<Arcrw<HashSet<Id>>>,

        default_filter_viewed: bool,
        default_filter_anonymous: bool,
        default_filter_adult: bool,
    ) -> Self {
        Self {
            default_report_thresholds,
            default_hidden_topics,
            default_hidden_users,
            default_hidden_articles,
            default_hidden_comments,

            default_filter_anonymous,
            default_filter_adult,
            default_filter_viewed,

            db_article_flags: DatabaseGetArticleFlags::new(db.clone()),
            db_comment_flags: DatabaseGetCommentFlags::new(db.clone()),
            db_user_flags: DatabaseGetUserFlags::new(db.clone()),
            db_user_reports_thresholds: DatabaseGetUserReportThresholds::new(db.clone()),
            db_user_hidden_topics: DatabaseGetUserHiddenTopics::new(db.clone()),
            db_user_hidden_users: DatabaseGetUserHiddenUsers::new(db.clone()),
            db_user_hidden_articles: DatabaseGetUserHiddenArticles::new(db.clone()),
            db_user_hidden_comments: DatabaseGetUserHiddenComments::new(db.clone()),
            db_user_viewed_articles: DatabaseGetUserViewedArticles::new(db.clone()),
            db_user_viewed_comments: DatabaseGetUserViewedComments::new(db),
        }
    }

    pub fn default_article_filter(
        &self,
        visible_articles: Option<HashSet<Id>>,
        filter_reported: Option<bool>,
        filter_anonymous: Option<bool>,
        filter_adult: Option<bool>,
        filter_hidden_topics: Option<bool>,
        filter_hidden_users: Option<bool>,
        filter_hidden_articles: Option<bool>,
    ) -> ArticleFilterReadLock {
        ArticleFilterReadLock {
            db_article_flags: self.db_article_flags.clone(),
            visible_articles,
            viewed_articles: None,
            hidden_articles: FilterHandler::id_set(
                filter_hidden_articles,
                self.default_hidden_articles.clone(),
            ),
            hidden_topics: FilterHandler::id_set(
                filter_hidden_topics,
                self.default_hidden_topics.clone(),
            ),
            hidden_users: FilterHandler::id_set(
                filter_hidden_users,
                self.default_hidden_users.clone(),
            ),
            author_filter: self.author_filter(filter_reported),
            filter_anonymous: filter_anonymous.unwrap_or(self.default_filter_anonymous),
            filter_adult: filter_adult.unwrap_or(self.default_filter_adult),
        }
    }

    pub fn default_comment_filter(
        &self,
        visible_comments: Option<HashSet<Id>>,
        filter_reported: Option<bool>,
        filter_anonymous: Option<bool>,
        filter_adult: Option<bool>,
        filter_hidden_users: Option<bool>,
        filter_hidden_comments: Option<bool>,
    ) -> CommentFilterReadLock {
        CommentFilterReadLock {
            db_comment_flags: self.db_comment_flags.clone(),
            visible_comments,
            viewed_comments: None,
            hidden_comments: FilterHandler::id_set(
                filter_hidden_comments,
                self.default_hidden_comments.clone(),
            ),
            hidden_users: FilterHandler::id_set(
                filter_hidden_users,
                self.default_hidden_users.clone(),
            ),
            author_filter: self.author_filter(filter_reported),
            filter_anonymous: filter_anonymous.unwrap_or(self.default_filter_anonymous),
            filter_adult: filter_adult.unwrap_or(self.default_filter_adult),
        }
    }

    pub async fn user_article_filter(
        &self,
        user_id: Id,
        visible_articles: Option<HashSet<Id>>,
        filter_reported: Option<bool>,
        filter_viewed: Option<bool>,
        filter_anonymous: Option<bool>,
        filter_adult: Option<bool>,
        filter_hidden_topics: Option<bool>,
        filter_hidden_users: Option<bool>,
        filter_hidden_articles: Option<bool>,
    ) -> Result<ArticleFilterReadLock, Error> {
        let (viewed_articles, hidden_articles, hidden_topics, hidden_users, author_filter) = try_join!(
            FilterHandler::user_id_set(
                filter_viewed,
                user_id,
                self.db_user_viewed_articles.clone(),
            ),
            FilterHandler::user_id_set_with_default(
                filter_hidden_articles,
                user_id,
                self.db_user_hidden_articles.clone(),
                self.default_hidden_articles.clone(),
            ),
            FilterHandler::user_id_set_with_default(
                filter_hidden_topics,
                user_id,
                self.db_user_hidden_topics.clone(),
                self.default_hidden_topics.clone(),
            ),
            FilterHandler::user_id_set_with_default(
                filter_hidden_users,
                user_id,
                self.db_user_hidden_users.clone(),
                self.default_hidden_users.clone(),
            ),
            self.user_author_filter(filter_reported, user_id)
        )?;

        Ok(ArticleFilterReadLock {
            db_article_flags: self.db_article_flags.clone(),
            visible_articles,
            viewed_articles,
            hidden_articles,
            hidden_topics,
            hidden_users,
            author_filter,
            filter_anonymous: filter_anonymous.unwrap_or(self.default_filter_anonymous),
            filter_adult: filter_adult.unwrap_or(self.default_filter_adult),
        })
    }

    pub async fn user_comment_filter(
        &self,
        user_id: Id,
        visible_comments: Option<HashSet<Id>>,
        filter_reported: Option<bool>,
        filter_viewed: Option<bool>,
        filter_anonymous: Option<bool>,
        filter_adult: Option<bool>,
        filter_hidden_users: Option<bool>,
        filter_hidden_comments: Option<bool>,
    ) -> Result<CommentFilterReadLock, Error> {
        let (viewed_comments, hidden_comments, hidden_users, author_filter) = try_join!(
            FilterHandler::user_id_set(
                filter_viewed,
                user_id,
                self.db_user_viewed_comments.clone(),
            ),
            FilterHandler::user_id_set_with_default(
                filter_hidden_comments,
                user_id,
                self.db_user_hidden_comments.clone(),
                self.default_hidden_comments.clone(),
            ),
            FilterHandler::user_id_set_with_default(
                filter_hidden_users,
                user_id,
                self.db_user_hidden_users.clone(),
                self.default_hidden_users.clone(),
            ),
            self.user_author_filter(filter_reported, user_id)
        )?;

        Ok(CommentFilterReadLock {
            db_comment_flags: self.db_comment_flags.clone(),
            visible_comments,
            viewed_comments,
            hidden_comments,
            hidden_users,
            author_filter,
            filter_anonymous: filter_anonymous.unwrap_or(self.default_filter_anonymous),
            filter_adult: filter_adult.unwrap_or(self.default_filter_adult),
        })
    }

    fn author_filter(&self, filter_reported: Option<bool>) -> Option<AuthorFilter> {
        if let Some(false) = filter_reported {
            return None;
        }

        self.default_report_thresholds
            .as_ref()
            .map(|s| AuthorFilter {
                db_user_flags: self.db_user_flags.clone(),
                report_thresholds: s.read().clone(),
            })
    }

    async fn user_author_filter(
        &self,
        filter_reported: Option<bool>,
        user_id: Id,
    ) -> Result<Option<AuthorFilter>, Error> {
        if let Some(false) = filter_reported {
            return Ok(None);
        }

        if let Some(mut report_thresholds) = self
            .default_report_thresholds
            .as_ref()
            .map(|s| s.read().clone())
        {
            for (id, threshold) in self.db_user_reports_thresholds.get(user_id).await? {
                report_thresholds[id as usize] = threshold
            }

            Ok(Some(AuthorFilter {
                db_user_flags: self.db_user_flags.clone(),
                report_thresholds,
            }))
        } else {
            Ok(None)
        }
    }

    async fn user_id_set<T: DatabaseGetIdSet>(
        filter_ids: Option<bool>,
        user_id: Id,
        db_ids_getter: T,
    ) -> Result<Option<Arcrw<HashSet<i64>>>, Error> {
        if let Some(false) = filter_ids {
            return Ok(None);
        }

        db_ids_getter.get(user_id).await
    }

    async fn user_id_set_with_default<T: DatabaseGetIdSet>(
        filter_ids: Option<bool>,
        user_id: Id,
        db_ids_getter: T,
        default_ids: Option<Arcrw<HashSet<Id>>>,
    ) -> Result<Option<Arcrw<HashSet<i64>>>, Error> {
        if let Some(false) = filter_ids {
            return Ok(None);
        }

        if let Some(ids) = db_ids_getter.get(user_id).await? {
            return Ok(Some(ids));
        }

        Ok(default_ids)
    }

    fn id_set(
        filter_ids: Option<bool>,
        default_ids: Option<Arcrw<HashSet<Id>>>,
    ) -> Option<Arcrw<HashSet<i64>>> {
        if let Some(false) = filter_ids {
            return None;
        }

        default_ids
    }
}

pub struct ArticleFilter {
    db_article_flags: DatabaseGetArticleFlags,
    author_filter: Option<AuthorFilter>,
    visible_articles: Option<HashSet<Id>>,
    viewed_articles: Option<ArcRwLockReadGuard<RawRwLock, HashSet<Id>>>,
    hidden_topics: Option<ArcRwLockReadGuard<RawRwLock, HashSet<Id>>>,
    hidden_users: Option<ArcRwLockReadGuard<RawRwLock, HashSet<Id>>>,
    hidden_articles: Option<ArcRwLockReadGuard<RawRwLock, HashSet<Id>>>,
    filter_anonymous: bool,
    filter_adult: bool,
}

impl ArticleFilter {
    pub async fn filter(&self, id: Id) -> bool {
        if self
            .visible_articles
            .as_ref()
            .is_some_and(|visible_articles| visible_articles.contains(&id))
        {
            return false;
        }

        if self
            .viewed_articles
            .as_ref()
            .is_some_and(|viewed_articles| viewed_articles.contains(&id))
        {
            return false;
        }

        if self
            .hidden_articles
            .as_ref()
            .is_some_and(|hidden_articles| hidden_articles.contains(&id))
        {
            return false;
        }

        if let Ok(article_flags) = self.db_article_flags.get(id).await {
            if self.filter_adult && article_flags.adult {
                return false;
            }

            if self.filter_anonymous && article_flags.anonymous {
                return false;
            }

            if self
                .hidden_users
                .as_ref()
                .is_some_and(|hidden_authors| hidden_authors.contains(&article_flags.user_id))
            {
                return false;
            }

            if let Some(hidden_topics) = self.hidden_topics.as_ref() {
                if article_flags
                    .topic_ids
                    .iter()
                    .any(|topic_id| hidden_topics.contains(&topic_id))
                {
                    return false;
                }
            }

            if let Some(author_filter) = self.author_filter.as_ref() {
                if !author_filter.filter(article_flags.user_id).await {
                    return false;
                }
            }

            true
        } else {
            false
        }
    }
}

pub struct CommentFilter {
    db_comment_flags: DatabaseGetCommentFlags,
    author_filter: Option<AuthorFilter>,
    visible_comments: Option<HashSet<Id>>,
    viewed_comments: Option<ArcRwLockReadGuard<RawRwLock, HashSet<Id>>>,
    hidden_users: Option<ArcRwLockReadGuard<RawRwLock, HashSet<Id>>>,
    hidden_comments: Option<ArcRwLockReadGuard<RawRwLock, HashSet<Id>>>,
    filter_anonymous: bool,
    filter_adult: bool,
}

impl CommentFilter {
    pub async fn filter(&self, id: Id) -> bool {
        if self
            .visible_comments
            .as_ref()
            .is_some_and(|visible_comments| visible_comments.contains(&id))
        {
            return false;
        }

        if self
            .viewed_comments
            .as_ref()
            .is_some_and(|viewed_comments| viewed_comments.contains(&id))
        {
            return false;
        }

        if self
            .hidden_comments
            .as_ref()
            .is_some_and(|hidden_comments| hidden_comments.contains(&id))
        {
            return false;
        }

        if let Ok(comment_flags) = self.db_comment_flags.get(id).await {
            if self.filter_adult && comment_flags.adult {
                return false;
            }

            if self.filter_anonymous && comment_flags.anonymous {
                return false;
            }

            if self
                .hidden_users
                .as_ref()
                .is_some_and(|hidden_authors| hidden_authors.contains(&comment_flags.user_id))
            {
                return false;
            }

            if let Some(author_filter) = self.author_filter.as_ref() {
                if !author_filter.filter(comment_flags.user_id).await {
                    return false;
                }
            }

            true
        } else {
            false
        }
    }
}

struct AuthorFilter {
    db_user_flags: DatabaseGetUserFlags,
    report_thresholds: Vec<f64>,
}

impl AuthorFilter {
    pub async fn filter(&self, id: Id) -> bool {
        if let Ok(user_flags) = self.db_user_flags.get(id).await {
            if self
                .report_thresholds
                .iter()
                .zip(user_flags.reports_count)
                .any(|(&report_threshold, report_count)| {
                    lower_bound_wilson_95ci(report_count, user_flags.views) > report_threshold
                })
            {
                return false;
            }

            true
        } else {
            false
        }
    }
}

pub struct ArticleFilterReadLock {
    db_article_flags: DatabaseGetArticleFlags,
    author_filter: Option<AuthorFilter>,
    visible_articles: Option<HashSet<Id>>,
    viewed_articles: Option<Arcrw<HashSet<i64>>>,
    hidden_topics: Option<Arcrw<HashSet<i64>>>,
    hidden_users: Option<Arcrw<HashSet<i64>>>,
    hidden_articles: Option<Arcrw<HashSet<i64>>>,
    filter_anonymous: bool,
    filter_adult: bool,
}

impl ArticleFilterReadLock {
    pub fn read(self) -> ArticleFilter {
        ArticleFilter {
            db_article_flags: self.db_article_flags,
            author_filter: self.author_filter,
            visible_articles: self.visible_articles,
            viewed_articles: self.viewed_articles.map(|s| s.read_arc()),
            hidden_topics: self.hidden_topics.map(|s| s.read_arc()),
            hidden_users: self.hidden_users.map(|s| s.read_arc()),
            hidden_articles: self.hidden_articles.map(|s| s.read_arc()),
            filter_anonymous: self.filter_anonymous,
            filter_adult: self.filter_adult,
        }
    }
}

pub struct CommentFilterReadLock {
    db_comment_flags: DatabaseGetCommentFlags,
    author_filter: Option<AuthorFilter>,
    visible_comments: Option<HashSet<Id>>,
    viewed_comments: Option<Arcrw<HashSet<i64>>>,
    hidden_users: Option<Arcrw<HashSet<i64>>>,
    hidden_comments: Option<Arcrw<HashSet<i64>>>,
    filter_anonymous: bool,
    filter_adult: bool,
}

impl CommentFilterReadLock {
    pub fn read(self) -> CommentFilter {
        CommentFilter {
            db_comment_flags: self.db_comment_flags,
            author_filter: self.author_filter,
            visible_comments: self.visible_comments,
            viewed_comments: self.viewed_comments.map(|s| s.read_arc()),
            hidden_users: self.hidden_users.map(|s| s.read_arc()),
            hidden_comments: self.hidden_comments.map(|s| s.read_arc()),
            filter_anonymous: self.filter_anonymous,
            filter_adult: self.filter_adult,
        }
    }
}
