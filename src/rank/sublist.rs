use crate::{Id, Time};
use std::cmp::{min, Reverse};
use std::collections::{BinaryHeap, HashMap};
use std::iter::Rev;
use std::slice::Iter;

fn pow2(n: usize) -> usize {
    1usize << n
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
struct Entry<Score> {
    score: Score,
    id: Id,
}

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct HeapEntry<Score> {
    score: Score,
    pub id: Id,
    depth: usize,
    pub start: usize,
    pub end: usize,
}

pub struct SublistRankerTimes {
    times: Vec<Time>,
}

impl SublistRankerTimes {
    fn start_index(&self, time: Time) -> usize {
        match self.times.binary_search(&time) {
            Ok(i) => i,
            Err(i) => i,
        }
    }

    fn end_index(&self, time: Time) -> usize {
        match self.times.binary_search(&time) {
            Ok(j) => j,
            Err(j) => j - 1,
        }
    }

    pub fn get_indices(
        &self,
        start_time: Option<Time>,
        end_time: Option<Time>,
    ) -> (Option<usize>, Option<usize>) {
        let i = if let Some(start_time) = start_time {
            Some(self.start_index(start_time))
        } else {
            None
        };

        let j = if let Some(end_time) = end_time {
            Some(self.end_index(end_time))
        } else {
            None
        };

        (i, j)
    }
}

pub struct SublistTimeRanker {
    entries: Vec<Id>,
}

impl SublistTimeRanker {
    pub fn len(&self) -> usize {
        self.entries.len()
    }

    pub fn new(mut ids: Vec<Id>) -> SublistTimeRanker {
        ids.sort_unstable();

        SublistTimeRanker { entries: ids }
    }

    pub fn push(&mut self, id: Id) {
        self.entries.push(id);
    }

    pub fn get(&self, i: Option<usize>, j: Option<usize>) -> Rev<Iter<Id>> {
        let i = i.unwrap_or(0);
        let j = j.unwrap_or(self.len());

        self.entries[i..j].iter().rev()
    }

    pub fn get_rev(&self, i: Option<usize>, j: Option<usize>) -> Iter<Id> {
        let i = i.unwrap_or(0);
        let j = j.unwrap_or(self.len());

        self.entries[i..j].iter()
    }
}

pub struct SublistScoreRanker<S> {
    entries: Vec<Vec<Entry<S>>>,
    id_to_indices: HashMap<Id, Vec<usize>>,
}

impl<S: Copy + Ord> SublistScoreRanker<S> {
    pub fn len(&self) -> usize {
        self.id_to_indices.len()
    }

    pub fn next_heap_entry(&self, heap_entry: HeapEntry<S>) -> HeapEntry<S> {
        self.first_heap_entry(heap_entry.depth, heap_entry.start + 1, heap_entry.end)
    }

    pub fn prev_heap_entry(&self, heap_entry: HeapEntry<S>) -> HeapEntry<S> {
        self.last_heap_entry(heap_entry.depth, heap_entry.start, heap_entry.end - 1)
    }

    fn height(&self) -> usize {
        1 + self.len().ilog2() as usize
    }

    fn contains(&self, id: Id) -> bool {
        self.id_to_indices.contains_key(&id)
    }

    fn first_heap_entry(&self, depth: usize, start: usize, end: usize) -> HeapEntry<S> {
        let Entry { score, id } = self.entries[depth][start];
        HeapEntry {
            score,
            id,
            depth,
            start,
            end,
        }
    }

    fn last_heap_entry(&self, depth: usize, start: usize, end: usize) -> HeapEntry<S> {
        let Entry { score, id } = self.entries[depth][end];
        HeapEntry {
            score,
            id,
            depth,
            start,
            end,
        }
    }

    pub fn new(ids: &[Id], scores: &[S]) -> SublistScoreRanker<S> {
        debug_assert!(ids.len() == scores.len() && ids.len() > 0);

        let len = ids.len();
        let height = 1 + len.ilog2() as usize;
        let mut entries = Vec::with_capacity(height);

        let mut row_entries: Vec<_> = ids
            .into_iter()
            .zip(scores)
            .map(|(&id, &score)| Entry { score, id })
            .collect();
        row_entries.sort_unstable_by_key(|&entry| entry.id);
        entries.push(row_entries.clone());

        let mut id_to_indices = HashMap::with_capacity(len);
        for (i, entry) in row_entries.iter().enumerate() {
            let mut indices = Vec::with_capacity(height);
            indices.push(i);
            id_to_indices.insert(entry.id, indices);
        }

        let mut factor = 2;
        for _ in 1..height {
            let mut i = 0;
            while i <= len {
                let j = i + factor;
                row_entries[i..j].sort_unstable();
                row_entries[i..j].reverse();
                i = j;
            }

            for (i, entry) in row_entries.iter().enumerate() {
                id_to_indices.get_mut(&entry.id).unwrap().push(i);
            }

            entries.push(row_entries.clone());
            factor *= 2;
        }

        SublistScoreRanker {
            entries,
            id_to_indices,
        }
    }

    pub fn push(&mut self, id: Id, score: S, time: Time) {
        debug_assert!(!self.contains(id));

        self.id_to_indices
            .insert(id, vec![self.len(); self.height()]);

        for vec in self.entries.iter_mut() {
            vec.push(Entry { score, id });
        }

        self.update(id, score);

        if self.len().is_power_of_two() {
            self.grow();
        }
    }

    pub fn update(&mut self, id: Id, score: S) {
        debug_assert!(self.contains(id));

        let mut factor = 2;
        for (i, mut j) in self
            .id_to_indices
            .get(&id)
            .unwrap()
            .clone()
            .into_iter()
            .enumerate()
            .skip(1)
        {
            self.entries[i][j].score = score;
            let min_index = j - j % factor;
            let max_index = min(min_index + factor - 1, self.len() - 1);

            while j > min_index && self.entries[i][j - 1] < self.entries[i][j] {
                self.swap_left_neighbour(i, j);
                j -= 1;
            }

            while j < max_index && self.entries[i][j] < self.entries[i][j + 1] {
                self.swap_left_neighbour(i, j + 1);
                j += 1;
            }

            factor *= 2;
        }
    }

    fn swap_left_neighbour(&mut self, i: usize, j: usize) {
        self.id_to_indices.get_mut(&self.entries[i][j].id).unwrap()[i] -= 1;
        self.id_to_indices
            .get_mut(&self.entries[i][j - 1].id)
            .unwrap()[i] += 1;

        self.entries[i].swap(j, j - 1);
    }

    fn grow(&mut self) {
        debug_assert!(self.len().is_power_of_two() && self.len() > 1);

        let mut k = 0;
        let mut i = 0;
        let mut j = self.len() / 2;
        let last_entries = self.entries.last().unwrap();
        let mut new_entries = Vec::with_capacity(last_entries.capacity());

        while i < self.len() / 2 && j < self.len() {
            if last_entries[i] > last_entries[j] {
                new_entries.push(last_entries[i]);
                self.id_to_indices
                    .get_mut(&last_entries[i].id)
                    .unwrap()
                    .push(k);
                i += 1;
            } else {
                new_entries.push(last_entries[j]);
                self.id_to_indices
                    .get_mut(&last_entries[j].id)
                    .unwrap()
                    .push(k);
                j += 1;
            }
            k += 1;
        }

        while i < self.len() / 2 {
            new_entries.push(last_entries[i]);
            self.id_to_indices
                .get_mut(&last_entries[i].id)
                .unwrap()
                .push(k);
            i += 1;
            k += 1;
        }

        while j < self.len() {
            new_entries.push(last_entries[j]);
            self.id_to_indices
                .get_mut(&last_entries[j].id)
                .unwrap()
                .push(k);
            j += 1;
            k += 1;
        }

        self.entries.push(new_entries);
    }

    pub fn get(&self, i: Option<usize>, j: Option<usize>) -> BinaryHeap<HeapEntry<S>> {
        let mut heap = BinaryHeap::with_capacity(2 * self.height());
        let i = i.unwrap_or(0);
        let j = j.unwrap_or(self.entries.len());
        if i > j {
            return heap;
        }

        {
            let mut x = 0;
            let mut y = self.len() - 1;
            let mut d = self.height() - 1;
            let mut m = pow2(d);

            while !(x <= i && y == j) {
                if j < m {
                    y = m - 1;
                    m = m - pow2(d - 1);
                } else {
                    if i <= x && m - 1 <= j {
                        heap.push(self.first_heap_entry(d, x, m - 1));
                    }
                    x = m;
                    m = m + pow2(d - 1);
                }
                d -= 1;
            }

            heap.push(self.first_heap_entry(d, x, y));
        }

        {
            let mut x = 0;
            let mut y = self.len() - 1;
            let mut d = self.height() - 1;
            let mut m = pow2(d);

            while !(x == i && y <= j) {
                if i < m {
                    if i <= m && y <= j {
                        heap.push(self.first_heap_entry(d, m, y));
                    }
                    y = m - 1;
                    m = m - pow2(d - 1);
                } else {
                    x = m;
                    m = m + pow2(d - 1);
                }
                d -= 1;
            }

            heap.push(self.first_heap_entry(d, x, y));
        }

        heap
    }

    pub fn get_rev(&self, i: Option<usize>, j: Option<usize>) -> BinaryHeap<Reverse<HeapEntry<S>>> {
        let mut heap = BinaryHeap::with_capacity(2 * self.height());
        let i = i.unwrap_or(0);
        let j = j.unwrap_or(self.entries.len());
        if i > j {
            return heap;
        }

        {
            let mut x = 0;
            let mut y = self.len() - 1;
            let mut d = self.height() - 1;
            let mut m = pow2(d);

            while !(x <= i && y == j) {
                if j < m {
                    y = m - 1;
                    m = m - pow2(d - 1);
                } else {
                    if i <= x && m - 1 <= j {
                        heap.push(Reverse(self.last_heap_entry(d, x, m - 1)))
                    }
                    x = m;
                    m = m + pow2(d - 1);
                }
                d -= 1;
            }

            heap.push(Reverse(self.last_heap_entry(d, x, y)))
        }

        {
            let mut x = 0;
            let mut y = self.len() - 1;
            let mut d = self.height() - 1;
            let mut m = pow2(d);

            while !(x == i && y <= j) {
                if i < m {
                    if i <= m && y <= j {
                        heap.push(Reverse(self.last_heap_entry(d, m, y)))
                    }
                    y = m - 1;
                    m = m - pow2(d - 1);
                } else {
                    x = m;
                    m = m + pow2(d - 1);
                }
                d -= 1;
            }

            heap.push(Reverse(self.last_heap_entry(d, x, y)))
        }

        heap
    }
}
