use serde::{Deserialize, Serialize};
use sqlx::{Decode, Encode, Postgres, Type};
use std::str::FromStr;

#[derive(Debug, PartialEq, Eq, Serialize, Deserialize, Clone, Copy)]
pub enum RankBy {
    Time,
    Views,
    Votes,
    Comments,
    VotesPerViews,
    CommentsPerViews,
}

// Implement sqlx::Type for RankBy
impl Type<Postgres> for RankBy {
    fn type_info() -> sqlx::postgres::PgTypeInfo {
        sqlx::postgres::PgTypeInfo::with_name("rank_by")
    }
}

// Implement sqlx::Encode for RankBy
impl<'q> Encode<'q, Postgres> for RankBy {
    fn encode_by_ref(&self, buf: &mut sqlx::postgres::PgArgumentBuffer) -> sqlx::encode::IsNull {
        let value = match self {
            RankBy::Time => "Time",
            RankBy::Views => "Views",
            RankBy::Votes => "Votes",
            RankBy::Comments => "Comments",
            RankBy::VotesPerViews => "VotesPerViews",
            RankBy::CommentsPerViews => "CommentsPerViews",
        };
        Encode::<Postgres>::encode(value, buf)
    }
}

// Implement sqlx::Decode for RankBy
impl<'r> Decode<'r, Postgres> for RankBy {
    fn decode(value: sqlx::postgres::PgValueRef<'r>) -> Result<Self, sqlx::error::BoxDynError> {
        let s = <&str as Decode<Postgres>>::decode(value)?;
        match s {
            "Time" => Ok(RankBy::Time),
            "Views" => Ok(RankBy::Views),
            "Votes" => Ok(RankBy::Votes),
            "Comments" => Ok(RankBy::Comments),
            "VotesPerViews" => Ok(RankBy::VotesPerViews),
            "CommentsPerViews" => Ok(RankBy::CommentsPerViews),
            _ => Err(format!("Unknown enum variant: {}", s).into()),
        }
    }
}

// Implement FromStr for easier parsing
impl FromStr for RankBy {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "Time" => Ok(RankBy::Time),
            "Views" => Ok(RankBy::Views),
            "Votes" => Ok(RankBy::Votes),
            "Comments" => Ok(RankBy::Comments),
            "VotesPerViews" => Ok(RankBy::VotesPerViews),
            "CommentsPerViews" => Ok(RankBy::CommentsPerViews),
            _ => Err(()),
        }
    }
}

pub enum RankBy {
    Time,
    Views,
    Votes,
    Comments,
    VotesPerViews,
    CommentsPerViews,
}
