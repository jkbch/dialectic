use super::sublist::{SublistScoreRanker, SublistTimeRanker};
use crate::db::getter::{
    DatabaseGetSublistRankerTimes, DatabaseGetSublistScoreRanker, DatabaseGetSublistTimeRanker,
};
use crate::filter::ArticleFilterReadLock;
use crate::{Arcrw, Id, Time};
use futures::prelude::*;
use rayon::prelude::*;
use sqlx::Error;
use std::cmp::Reverse;
use std::collections::BinaryHeap;
use std::marker::PhantomData;
use tokio::{join, try_join};

#[derive(Clone)]
pub struct ArticleRankerTimes<D: DatabaseGetSublistRankerTimes + Clone> {
    db_times: D,
}

impl<D: DatabaseGetSublistRankerTimes + Clone> ArticleRankerTimes<D> {
    pub fn new(db_times: D) -> Self {
        Self { db_times }
    }

    async fn get_indices(
        &self,
        topic_ids: &[Id],
        start_time: Option<Time>,
        end_time: Option<Time>,
    ) -> Result<Vec<(Option<usize>, Option<usize>)>, Error> {
        Ok(if start_time.is_some() || end_time.is_some() {
            let times = future::join_all(topic_ids.iter().cloned().map(|id| self.db_times.get(id)))
                .await
                .into_iter()
                .collect::<Result<Vec<_>, _>>()?;

            times
                .into_par_iter()
                .map(|times| times.read().get_indices(start_time, end_time))
                .collect()
        } else {
            vec![(None, None); topic_ids.len()]
        })
    }

    async fn single_get_indices(
        &self,
        topic_id: Id,
        start_time: Option<Time>,
        end_time: Option<Time>,
    ) -> Result<(Option<usize>, Option<usize>), Error> {
        Ok(if start_time.is_some() || end_time.is_some() {
            self.db_times
                .get(topic_id)
                .await?
                .read()
                .get_indices(start_time, end_time)
        } else {
            (None, None)
        })
    }
}

pub struct ArticleTimeRanker<
    D1: DatabaseGetSublistTimeRanker,
    D2: DatabaseGetSublistRankerTimes + Clone,
> {
    db_ranker: D1,
    times: ArticleRankerTimes<D2>,
    cutoff: usize,
}

impl<D1: DatabaseGetSublistTimeRanker, D2: DatabaseGetSublistRankerTimes + Clone>
    ArticleTimeRanker<D1, D2>
{
    pub fn new(db_ranker: D1, times: ArticleRankerTimes<D2>, cutoff: usize) -> Self {
        Self {
            db_ranker,
            times,
            cutoff,
        }
    }

    async fn get_rankers(&self, topic_ids: &[Id]) -> Result<Vec<Arcrw<SublistTimeRanker>>, Error> {
        future::join_all(topic_ids.iter().cloned().map(|id| self.db_ranker.get(id)))
            .await
            .into_iter()
            .collect::<Result<Vec<_>, _>>()
    }

    pub async fn single_get(
        &self,
        topic_id: Id,
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let ((i, j), ranker_lock) = try_join!(
            self.times
                .single_get_indices(topic_id, start_time, end_time),
            self.db_ranker.get(topic_id)
        )?;

        let mut article_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let ranker = ranker_lock.read();

        for &id in ranker.get(i, j).take(self.cutoff) {
            if filter.filter(id).await {
                article_ids.push(id);

                if article_ids.len() == amount {
                    break;
                }
            }
        }

        Ok(article_ids)
    }

    pub async fn single_get_rev(
        &self,
        topic_id: Id,
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let ((i, j), ranker_lock) = try_join!(
            self.times
                .single_get_indices(topic_id, start_time, end_time),
            self.db_ranker.get(topic_id)
        )?;

        let mut article_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let ranker = ranker_lock.read();

        for &id in ranker.get_rev(i, j).take(self.cutoff) {
            if filter.filter(id).await {
                article_ids.push(id);

                if article_ids.len() == amount {
                    break;
                }
            }
        }

        Ok(article_ids)
    }

    pub async fn get(
        &self,
        topic_ids: &[Id],
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let (indices, ranker_locks) = try_join!(
            self.times.get_indices(topic_ids, start_time, end_time),
            self.get_rankers(topic_ids),
        )?;

        let mut heap = BinaryHeap::with_capacity(topic_ids.len());
        let mut article_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let rankers: Vec<_> = ranker_locks.iter().map(|lock| lock.read()).collect();

        let mut iters: Vec<_> = rankers
            .iter()
            .zip(indices)
            .map(|(ranker, (i, j))| ranker.get(i, j))
            .collect();

        for (i, iter) in iters.iter_mut().enumerate() {
            if let Some(&entry) = iter.next() {
                heap.push((entry, i));
            }
        }

        let mut last_id = 0;
        for _ in 0..self.cutoff {
            if let Some((article_id, i)) = heap.pop() {
                if last_id != article_id && filter.filter(article_id).await {
                    article_ids.push(article_id);

                    if article_ids.len() == amount {
                        break;
                    }
                }
                if let Some(&entry) = iters[i].next() {
                    heap.push((entry, i));
                }
                last_id = article_id;
            } else {
                break;
            }
        }

        Ok(article_ids)
    }

    pub async fn get_rev(
        &self,
        topic_ids: &[Id],
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let (indices, ranker_locks) = try_join!(
            self.times.get_indices(topic_ids, start_time, end_time),
            self.get_rankers(topic_ids),
        )?;

        let mut heap = BinaryHeap::with_capacity(topic_ids.len());
        let mut article_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let rankers: Vec<_> = ranker_locks.iter().map(|lock| lock.read()).collect();

        let mut iters: Vec<_> = rankers
            .iter()
            .zip(indices)
            .map(|(ranker, (i, j))| ranker.get_rev(i, j))
            .collect();

        for (i, iter) in iters.iter_mut().enumerate() {
            if let Some(&entry) = iter.next() {
                heap.push(Reverse((entry, i)));
            }
        }

        let mut last_id = 0;
        for _ in 0..self.cutoff {
            if let Some(Reverse((article_id, i))) = heap.pop() {
                if last_id != article_id && filter.filter(article_id).await {
                    article_ids.push(article_id);

                    if article_ids.len() == amount {
                        break;
                    }
                }
                if let Some(&entry) = iters[i].next() {
                    heap.push(Reverse((entry, i)));
                }
                last_id = article_id;
            } else {
                break;
            }
        }

        Ok(article_ids)
    }
}

pub struct ArticleScoreRanker<
    S: Sync + Send + Copy + Ord,
    D1: DatabaseGetSublistScoreRanker<S>,
    D2: DatabaseGetSublistRankerTimes + Clone,
> {
    db_ranker: D1,
    times: ArticleRankerTimes<D2>,
    cutoff: usize,
    phantom_score: PhantomData<S>,
}

impl<
        S: Sync + Send + Copy + Ord,
        D1: DatabaseGetSublistScoreRanker<S>,
        D2: DatabaseGetSublistRankerTimes + Clone,
    > ArticleScoreRanker<S, D1, D2>
{
    pub fn new(db_ranker: D1, times: ArticleRankerTimes<D2>, cutoff: usize) -> Self {
        Self {
            db_ranker,
            times,
            cutoff,
            phantom_score: PhantomData,
        }
    }

    async fn get_rankers(
        &self,
        topic_ids: &[Id],
    ) -> Result<Vec<Arcrw<SublistScoreRanker<S>>>, Error> {
        future::join_all(topic_ids.iter().cloned().map(|id| self.db_ranker.get(id)))
            .await
            .into_iter()
            .collect::<Result<Vec<_>, _>>()
    }

    pub async fn single_get(
        &self,
        topic_id: Id,
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let ((i, j), ranker_lock) = try_join!(
            self.times
                .single_get_indices(topic_id, start_time, end_time),
            self.db_ranker.get(topic_id)
        )?;

        let mut article_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let ranker = ranker_lock.read();
        let mut heap = ranker.get(i, j);

        for _ in 0..self.cutoff {
            if let Some(heap_entry) = heap.pop() {
                if filter.filter(heap_entry.id).await {
                    article_ids.push(heap_entry.id);

                    if article_ids.len() == amount {
                        break;
                    }
                }

                if heap_entry.start < heap_entry.end {
                    heap.push(ranker.next_heap_entry(heap_entry));
                }
            } else {
                break;
            }
        }

        Ok(article_ids)
    }

    pub async fn single_get_rev(
        &self,
        topic_id: Id,
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let ((i, j), ranker_lock) = try_join!(
            self.times
                .single_get_indices(topic_id, start_time, end_time),
            self.db_ranker.get(topic_id)
        )?;

        let mut article_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let ranker = ranker_lock.read();
        let mut heap = ranker.get_rev(i, j);

        for _ in 0..self.cutoff {
            if let Some(Reverse(heap_entry)) = heap.pop() {
                if filter.filter(heap_entry.id).await {
                    article_ids.push(heap_entry.id);

                    if article_ids.len() == amount {
                        break;
                    }
                }

                if heap_entry.start < heap_entry.end {
                    heap.push(Reverse(ranker.prev_heap_entry(heap_entry)));
                }
            } else {
                break;
            }
        }

        Ok(article_ids)
    }

    pub async fn get(
        &self,
        topic_ids: &[Id],
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let (indices, ranker_locks) = try_join!(
            self.times.get_indices(topic_ids, start_time, end_time),
            self.get_rankers(topic_ids),
        )?;

        let mut top_heap = BinaryHeap::with_capacity(topic_ids.len());
        let mut article_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let rankers: Vec<_> = ranker_locks.iter().map(|lock| lock.read()).collect();

        let mut heaps: Vec<_> = rankers
            .par_iter()
            .zip(indices)
            .map(|(ranker, (i, j))| ranker.get(i, j))
            .collect();

        for (i, (ranker, heap)) in rankers.iter().zip(heaps.iter_mut()).enumerate() {
            if let Some(heap_entry) = heap.pop() {
                top_heap.push((heap_entry, i));

                if heap_entry.start < heap_entry.end {
                    heap.push(ranker.next_heap_entry(heap_entry));
                }
            }
        }

        let mut last_id = 0;
        for _ in 0..self.cutoff {
            if let Some((heap_entry, i)) = top_heap.pop() {
                if last_id != heap_entry.id && filter.filter(heap_entry.id).await {
                    article_ids.push(heap_entry.id);

                    if article_ids.len() == amount {
                        break;
                    }
                }

                if heap_entry.start < heap_entry.end {
                    heaps[i].push(rankers[i].next_heap_entry(heap_entry));

                    if let Some(heap_entry) = heaps[i].pop() {
                        top_heap.push((heap_entry, i));
                    }
                }

                last_id = heap_entry.id;
            } else {
                break;
            }
        }

        Ok(article_ids)
    }

    pub async fn get_rev(
        &self,
        topic_ids: &[Id],
        start_time: Option<Time>,
        end_time: Option<Time>,
        amount: usize,
        filter_lock: ArticleFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let (indices, ranker_locks) = try_join!(
            self.times.get_indices(topic_ids, start_time, end_time),
            self.get_rankers(topic_ids),
        )?;

        let mut top_heap = BinaryHeap::with_capacity(topic_ids.len());
        let mut article_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let rankers: Vec<_> = ranker_locks.iter().map(|lock| lock.read()).collect();

        let mut heaps: Vec<_> = rankers
            .par_iter()
            .zip(indices)
            .map(|(ranker, (i, j))| ranker.get_rev(i, j))
            .collect();

        for (i, (ranker, heap)) in rankers.iter().zip(heaps.iter_mut()).enumerate() {
            if let Some(Reverse(heap_entry)) = heap.pop() {
                top_heap.push(Reverse((heap_entry, i)));

                if heap_entry.start < heap_entry.end {
                    heap.push(Reverse(ranker.prev_heap_entry(heap_entry)));
                }
            }
        }

        let mut last_id = 0;
        for _ in 0..self.cutoff {
            if let Some(Reverse((heap_entry, i))) = top_heap.pop() {
                if last_id != heap_entry.id && filter.filter(heap_entry.id).await {
                    article_ids.push(heap_entry.id);

                    if article_ids.len() == amount {
                        break;
                    }
                }

                if heap_entry.start < heap_entry.end {
                    heaps[i].push(Reverse(rankers[i].prev_heap_entry(heap_entry)));

                    if let Some(Reverse(heap_entry)) = heaps[i].pop() {
                        top_heap.push(Reverse((heap_entry, i)));
                    }
                }

                last_id = heap_entry.id;
            } else {
                break;
            }
        }

        Ok(article_ids)
    }
}
