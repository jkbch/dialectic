use crate::Id;
use std::collections::HashMap;

#[derive(PartialEq, Eq, PartialOrd, Ord, Clone, Copy)]
pub struct EntryNode<Score> {
    score: Score,
    pub id: Id,
    parent: Option<usize>,
    pub first_child: Option<usize>,
    pub last_child: Option<usize>,
    pub next_sibling: Option<usize>,
    pub prev_sibling: Option<usize>,
}

pub struct TreeRanker<S> {
    entries: Vec<EntryNode<S>>,
    id_to_index: HashMap<Id, usize>,
    root_first_child: usize,
    root_last_child: usize,
}

impl<S: Copy + Ord> TreeRanker<S> {
    pub fn len(&self) -> usize {
        self.entries.len()
    }

    fn contains(&self, id: Id) -> bool {
        self.id_to_index.contains_key(&id)
    }

    pub fn entry_by_id(&self, id: Id) -> EntryNode<S> {
        self.entries[*self.id_to_index.get(&id).unwrap()]
    }

    pub fn entry(&self, i: usize) -> EntryNode<S> {
        self.entries[i]
    }

    pub fn root_first_child(&self) -> EntryNode<S> {
        self.entries[self.root_first_child]
    }

    pub fn root_last_child(&self) -> EntryNode<S> {
        self.entries[self.root_last_child]
    }

    pub fn new(ids: &[Id], parent_ids: &[Option<Id>], scores: &[S]) -> TreeRanker<S> {
        debug_assert!(
            ids.len() == scores.len() && ids.len() == parent_ids.len() && !ids.is_empty()
        );

        let mut scores_ids_parent_ids: Vec<_> = scores.iter().zip(ids).zip(parent_ids).collect();
        scores_ids_parent_ids.sort_unstable();
        scores_ids_parent_ids.reverse();

        let id_to_index: HashMap<_, _> = scores_ids_parent_ids
            .iter()
            .enumerate()
            .map(|(i, &((_, &id), _))| (id, i))
            .collect();

        let mut entries: Vec<_> = scores_ids_parent_ids
            .iter()
            .map(|((&score, &id), parent_id)| match parent_id {
                Some(parent_id) => EntryNode {
                    score,
                    id,
                    parent: Some(*id_to_index.get(parent_id).unwrap()),
                    first_child: None,
                    last_child: None,
                    next_sibling: None,
                    prev_sibling: None,
                },
                None => EntryNode {
                    score,
                    id,
                    parent: None,
                    first_child: None,
                    last_child: None,
                    next_sibling: None,
                    prev_sibling: None,
                },
            })
            .collect();

        let mut parent_next_childs = vec![None; entries.len()];
        let mut root_first_child = None;
        let mut root_next_child = None;

        for i in 0..entries.len() {
            if let Some(parent) = entries[i].parent {
                if let Some(next_child) = parent_next_childs[parent] {
                    entries[i].prev_sibling = Some(next_child);
                    entries[next_child].next_sibling = Some(i);
                } else {
                    entries[parent].first_child = Some(i);
                }

                entries[parent].last_child = Some(i);
                parent_next_childs[parent] = Some(i);
            } else {
                entries[i].prev_sibling = root_next_child;

                if let Some(root_next_child) = root_next_child {
                    entries[root_next_child].next_sibling = Some(i);
                } else {
                    root_first_child = Some(i);
                }

                root_next_child = Some(i);
            }
        }

        TreeRanker {
            id_to_index,
            entries,
            root_first_child: root_first_child.unwrap(),
            root_last_child: root_next_child.unwrap(),
        }
    }

    pub fn update(&mut self, id: Id, score: S) {
        debug_assert!(self.contains(id));

        let mut i = *self.id_to_index.get(&id).unwrap();
        self.entries[i].score = score;

        while let Some(next_sibling) = self.entries[i].next_sibling {
            if self.entries[i] >= self.entries[next_sibling] {
                break;
            }
            self.swap_siblings(i, next_sibling);
            i = next_sibling;
        }

        while let Some(prev_sibling) = self.entries[i].prev_sibling {
            if self.entries[prev_sibling] >= self.entries[i] {
                break;
            }
            self.swap_siblings(prev_sibling, i);
            i = prev_sibling;
        }
    }

    fn swap_siblings(&mut self, prev_sibling: usize, next_sibling: usize) {
        self.entries[prev_sibling].next_sibling = self.entries[next_sibling].next_sibling;
        self.entries[next_sibling].prev_sibling = self.entries[prev_sibling].prev_sibling;

        self.entries[prev_sibling].prev_sibling = Some(next_sibling);
        self.entries[next_sibling].next_sibling = Some(prev_sibling);

        if let Some(parent) = self.entries[prev_sibling].parent {
            if let Some(first_child) = self.entries[parent].first_child {
                if first_child == prev_sibling {
                    self.entries[parent].first_child = Some(next_sibling);
                }
            }

            if let Some(last_child) = self.entries[parent].last_child {
                if last_child == next_sibling {
                    self.entries[parent].last_child = Some(prev_sibling);
                }
            }
        } else {
            if self.root_first_child == prev_sibling {
                self.root_first_child = next_sibling;
            }

            if self.root_last_child == next_sibling {
                self.root_last_child = prev_sibling;
            }
        }
    }

    pub fn push(&mut self, id: Id, parent_id: Option<Id>, score: S) {
        debug_assert!(!self.contains(id));

        let i = self.entries.len();

        let node_entry = match parent_id {
            Some(parent_id) => {
                let parent = *self.id_to_index.get(&parent_id).unwrap();

                let node_entry = EntryNode {
                    score,
                    id,
                    parent: Some(parent),
                    first_child: None,
                    last_child: None,
                    next_sibling: None,
                    prev_sibling: self.entries[parent].last_child,
                };

                if let Some(prev_sibling) = self.entries[parent].last_child {
                    self.entries[prev_sibling].next_sibling = Some(i);
                } else {
                    self.entries[parent].first_child = Some(i);
                }
                self.entries[parent].last_child = Some(i);

                node_entry
            }
            None => {
                let node_entry = EntryNode {
                    score,
                    id,
                    parent: None,
                    first_child: None,
                    last_child: None,
                    next_sibling: None,
                    prev_sibling: Some(self.root_last_child),
                };

                self.entries[self.root_last_child].next_sibling = Some(i);
                self.root_last_child = i;

                node_entry
            }
        };

        self.entries.push(node_entry);
        self.update(id, score);
    }
}
