use crate::{db::getter::DatabaseGetTreeRanker, filter::CommentFilterReadLock, Id};
use sqlx::Error;
use std::{cmp::Reverse, collections::BinaryHeap, marker::PhantomData};

pub struct CommentRanker<S: Copy + Ord + Sync + Send, D: DatabaseGetTreeRanker<S>> {
    db_ranker: D,
    cutoff: usize,
    phantom_score: PhantomData<S>,
}

impl<S: Copy + Ord + Sync + Send, D: DatabaseGetTreeRanker<S>> CommentRanker<S, D> {
    pub fn new(db_ranker: D, cutoff: usize) -> Self {
        Self {
            db_ranker,
            cutoff,
            phantom_score: PhantomData,
        }
    }

    pub async fn get(
        &self,
        article_id: Id,
        parent_comment_id: Option<Id>,
        child_comment_ids: &[Id],
        amount: usize,
        filter_lock: CommentFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let ranker_lock = self.db_ranker.get(article_id).await?;
        let mut heap = BinaryHeap::with_capacity(child_comment_ids.len() + 1);
        let mut comment_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let ranker = ranker_lock.read();

        if let Some(parent_comment_id) = parent_comment_id {
            if let Some(first_child) = ranker.entry_by_id(parent_comment_id).first_child {
                heap.push(ranker.entry(first_child));
            }
        } else {
            heap.push(ranker.root_first_child());
        }

        for &id in child_comment_ids {
            if let Some(first_child) = ranker.entry_by_id(id).first_child {
                heap.push(ranker.entry(first_child));
            }
        }

        for _ in 0..self.cutoff {
            if let Some(entry) = heap.pop() {
                if filter.filter(entry.id).await {
                    comment_ids.push(entry.id);

                    if comment_ids.len() == amount {
                        break;
                    }

                    if let Some(first_child) = entry.first_child {
                        heap.push(ranker.entry(first_child))
                    }
                }

                if let Some(next_sibling) = entry.next_sibling {
                    heap.push(ranker.entry(next_sibling))
                }
            } else {
                break;
            }
        }

        Ok(comment_ids)
    }

    pub async fn get_rev(
        &self,
        article_id: Id,
        parent_comment_id: Option<Id>,
        child_comment_ids: &[Id],
        amount: usize,
        filter_lock: CommentFilterReadLock,
    ) -> Result<Vec<Id>, Error> {
        let ranker_lock = self.db_ranker.get(article_id).await?;
        let mut heap = BinaryHeap::with_capacity(child_comment_ids.len() + 1);
        let mut comment_ids = Vec::with_capacity(amount);
        let filter = filter_lock.read();
        let ranker = ranker_lock.read();

        if let Some(parent_comment_id) = parent_comment_id {
            if let Some(last_child) = ranker.entry_by_id(parent_comment_id).last_child {
                heap.push(Reverse(ranker.entry(last_child)));
            }
        } else {
            heap.push(Reverse(ranker.root_last_child()));
        }

        for &id in child_comment_ids {
            if let Some(last_child) = ranker.entry_by_id(id).last_child {
                heap.push(Reverse(ranker.entry(last_child)));
            }
        }

        for _ in 0..self.cutoff {
            if let Some(Reverse(entry)) = heap.pop() {
                if filter.filter(entry.id).await {
                    comment_ids.push(entry.id);

                    if comment_ids.len() == amount {
                        break;
                    }

                    if let Some(last_child) = entry.last_child {
                        heap.push(Reverse(ranker.entry(last_child)))
                    }
                }

                if let Some(prev_sibling) = entry.prev_sibling {
                    heap.push(Reverse(ranker.entry(prev_sibling)))
                }
            } else {
                break;
            }
        }

        Ok(comment_ids)
    }
}
