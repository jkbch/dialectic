use crate::{db::login::AuthSession, rank::RankBy, state::AppState, web::internal_error, Id, Time};
use axum::{
    extract::{rejection::JsonRejection, Json, Path, Query, State},
    http::StatusCode,
};
use axum_macros::debug_handler;
use iso8601_duration::Duration;
use serde::Deserialize;
use std::{collections::HashSet, sync::Arc};

#[derive(Debug, Deserialize)]
pub struct ArticleFilterOption {
    amount: Option<usize>,
    skip: Option<usize>,
    rank_by: Option<RankBy>,
    visible_articles: Option<HashSet<Id>>,

    time_start: Option<Time>,
    time_period: Option<Duration>,

    filter_reported: Option<bool>,
    filter_viewed: Option<bool>,
    filter_anonymous: Option<bool>,
    filter_adult: Option<bool>,

    filter_hidden_topics: Option<bool>,
    filter_hidden_users: Option<bool>,
    filter_hidden_articles: Option<bool>,
}

#[debug_handler]
pub async fn get_topics(
    auth_session: AuthSession,
    State(state): State<Arc<AppState>>,
    Query(params): Query<ArticleFilterOption>,
    json: Result<Json<ArticleFilterOption>, JsonRejection>,
) -> Result<String, (StatusCode, String)> {
    let params = match json {
        Ok(Json(payload)) => Ok(payload),
        Err(JsonRejection::MissingJsonContentType(_)) => Ok(params),
        Err(rejection) => Err((rejection.status(), rejection.body_text())),
    }?;

    let filter = match auth_session.user {
        Some(user) => match state
            .filter
            .user_article_filter(
                user.id,
                params.visible_articles.clone(),
                params.filter_reported,
                params.filter_viewed,
                params.filter_anonymous,
                params.filter_adult,
                params.filter_hidden_topics,
                params.filter_hidden_users,
                params.filter_hidden_articles,
            )
            .await
        {
            Ok(filter) => Ok(filter),
            Err(e) => Err((StatusCode::INTERNAL_SERVER_ERROR, e.to_string())),
        }?,
        None => state.filter.default_article_filter(
            params.visible_articles.clone(),
            params.filter_reported,
            params.filter_anonymous,
            params.filter_adult,
            params.filter_hidden_topics,
            params.filter_hidden_users,
            params.filter_hidden_articles,
        ),
    };

    Ok(format!("{:?}", params))
}

pub async fn get_topics_name(
    auth_session: AuthSession,
    State(state): State<Arc<AppState>>,
    Path(topic_name): Path<String>,
    Query(params): Query<ArticleFilterOption>,
    json: Result<Json<ArticleFilterOption>, JsonRejection>,
) -> Result<String, (StatusCode, String)> {
    let params = match json {
        Ok(Json(payload)) => Ok(payload),
        Err(JsonRejection::MissingJsonContentType(_)) => Ok(params),
        Err(rejection) => Err((rejection.status(), rejection.body_text())),
    }?;

    Ok(format!("{:?}", params))
}
