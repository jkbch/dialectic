use askama::Template;

#[derive(Template)]
#[template(path = "index.html")]
pub struct IndexTemplate<'a> {
    name: &'a str,
}

pub async fn get_index() -> IndexTemplate<'static> {
    IndexTemplate { name: "Clara <3" }
}
