use parking_lot::RwLock;

use crate::{
    db::{
        getter::{
            DatabaseGetAuthorArticleCommentsPerViewsRanker, DatabaseGetAuthorArticleCommentsRanker,
            DatabaseGetAuthorArticleTimeRanker, DatabaseGetAuthorArticleTimes,
            DatabaseGetAuthorArticleViewsRanker, DatabaseGetAuthorArticleVotesPerViewsRanker,
            DatabaseGetAuthorArticleVotesRanker, DatabaseGetCommentCommentsPerViewsRanker,
            DatabaseGetCommentCommentsRanker, DatabaseGetCommentTimeRanker,
            DatabaseGetCommentViewsRanker, DatabaseGetCommentVotesPerViewsRanker,
            DatabaseGetCommentVotesRanker, DatabaseGetTopicArticleCommentsPerViewsRanker,
            DatabaseGetTopicArticleCommentsRanker, DatabaseGetTopicArticleTimeRanker,
            DatabaseGetTopicArticleTimes, DatabaseGetTopicArticleViewsRanker,
            DatabaseGetTopicArticleVotesPerViewsRanker, DatabaseGetTopicArticleVotesRanker,
        },
        Database,
    },
    filter::FilterHandler,
    rank::{ArticleRankerHandler, CommentRankerHandler},
    Arcrw, Id,
};
use std::{collections::HashSet, sync::Arc};

pub struct AppState {
    pub db: Arc<Database>,
    pub topic_articles_rank: ArticleRankerHandler<
        DatabaseGetTopicArticleTimeRanker,
        DatabaseGetTopicArticleViewsRanker,
        DatabaseGetTopicArticleVotesRanker,
        DatabaseGetTopicArticleCommentsRanker,
        DatabaseGetTopicArticleVotesPerViewsRanker,
        DatabaseGetTopicArticleCommentsPerViewsRanker,
        DatabaseGetTopicArticleTimes,
    >,
    pub author_articles_rank: ArticleRankerHandler<
        DatabaseGetAuthorArticleTimeRanker,
        DatabaseGetAuthorArticleViewsRanker,
        DatabaseGetAuthorArticleVotesRanker,
        DatabaseGetAuthorArticleCommentsRanker,
        DatabaseGetAuthorArticleVotesPerViewsRanker,
        DatabaseGetAuthorArticleCommentsPerViewsRanker,
        DatabaseGetAuthorArticleTimes,
    >,
    pub comments_rank: CommentRankerHandler<
        DatabaseGetCommentTimeRanker,
        DatabaseGetCommentViewsRanker,
        DatabaseGetCommentVotesRanker,
        DatabaseGetCommentCommentsRanker,
        DatabaseGetCommentVotesPerViewsRanker,
        DatabaseGetCommentCommentsPerViewsRanker,
    >,
    pub filter: FilterHandler,
}

impl AppState {
    pub fn new(
        db: Arc<Database>,
        cutoff: usize,

        default_report_thresholds: Option<RwLock<Vec<f64>>>,
        default_hidden_topics: Option<Arcrw<HashSet<Id>>>,
        default_hidden_users: Option<Arcrw<HashSet<Id>>>,
        default_hidden_articles: Option<Arcrw<HashSet<Id>>>,
        default_hidden_comments: Option<Arcrw<HashSet<Id>>>,

        default_filter_anonymous: bool,
        default_filter_adult: bool,
        default_filter_viewed: bool,
    ) -> Self {
        Self {
            topic_articles_rank: ArticleRankerHandler::new(
                DatabaseGetTopicArticleTimeRanker::new(db.clone()),
                DatabaseGetTopicArticleViewsRanker::new(db.clone()),
                DatabaseGetTopicArticleVotesRanker::new(db.clone()),
                DatabaseGetTopicArticleCommentsRanker::new(db.clone()),
                DatabaseGetTopicArticleVotesPerViewsRanker::new(db.clone()),
                DatabaseGetTopicArticleCommentsPerViewsRanker::new(db.clone()),
                DatabaseGetTopicArticleTimes::new(db.clone()),
                cutoff,
            ),
            author_articles_rank: ArticleRankerHandler::new(
                DatabaseGetAuthorArticleTimeRanker::new(db.clone()),
                DatabaseGetAuthorArticleViewsRanker::new(db.clone()),
                DatabaseGetAuthorArticleVotesRanker::new(db.clone()),
                DatabaseGetAuthorArticleCommentsRanker::new(db.clone()),
                DatabaseGetAuthorArticleVotesPerViewsRanker::new(db.clone()),
                DatabaseGetAuthorArticleCommentsPerViewsRanker::new(db.clone()),
                DatabaseGetAuthorArticleTimes::new(db.clone()),
                cutoff,
            ),
            comments_rank: CommentRankerHandler::new(
                DatabaseGetCommentTimeRanker::new(db.clone()),
                DatabaseGetCommentViewsRanker::new(db.clone()),
                DatabaseGetCommentVotesRanker::new(db.clone()),
                DatabaseGetCommentCommentsRanker::new(db.clone()),
                DatabaseGetCommentVotesPerViewsRanker::new(db.clone()),
                DatabaseGetCommentCommentsPerViewsRanker::new(db.clone()),
                cutoff,
            ),
            filter: FilterHandler::new(
                db.clone(),
                default_report_thresholds,
                default_hidden_topics,
                default_hidden_users,
                default_hidden_articles,
                default_hidden_comments,
                default_filter_viewed,
                default_filter_anonymous,
                default_filter_adult,
            ),
            db,
        }
    }
}
