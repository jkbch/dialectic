use self::cache::CacheBackend;
use crate::{
    db::cache::{CacheKey, CacheValue},
    rank::{
        sublist::{SublistRankerTimes, SublistScoreRanker, SublistTimeRanker},
        tree::TreeRanker,
        RankBy,
    },
    Arcrw, Id, Time,
};
use ordered_float::NotNan;
use paste::paste;
use quick_cache::sync::Cache;
use serde::{Deserialize, Serialize};
use sqlx::{prelude::FromRow, Error, Pool, Postgres};
use std::collections::HashSet;

mod cache;
pub mod getter;
pub mod login;

macro_rules! db_getter {
    ($EnumVariant:ident, $ReturnType:ty) => {
        paste! {
            db_getter!([<get_ $EnumVariant:snake>], $EnumVariant, $ReturnType);
        }
    };

    ($get_function:ident, $EnumVariant:ident, $ReturnType:ty) => {
        pub async fn $get_function(&self, id: Id) -> Result<$ReturnType, Error> {
            let cache_value = self
                .cache
                .get_or_insert_async(&CacheKey::$EnumVariant(id), self.backend.$get_function(id))
                .await?;

            match cache_value {
                CacheValue::$EnumVariant(value) => Ok(value),
                _ => panic!("Invalid cache value"),
            }
        }
    };
}

#[derive(Clone, Serialize, Deserialize, FromRow)]
pub struct LoginUser {
    pub id: i64,
    pub username: String,
    password: String,

    rank_by: Option<RankBy>,
    time_start: Option<Time>,
    time_period: Option<i64>,

    filter_viewed_articles: Option<bool>,
    filter_reported_users: Option<bool>,
    filter_hidden_topics: Option<bool>,
    filter_hidden_users: Option<bool>,
    filter_hidden_articles: Option<bool>,
    filter_anonymous: Option<bool>,
    filter_adult: Option<bool>,
}

// Here we've implemented `Debug` manually to avoid accidentally logging the
// password hash.
impl std::fmt::Debug for LoginUser {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("User")
            .field("id", &self.id)
            .field("username", &self.username)
            .field("password", &"[redacted]")
            .finish()
    }
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct ArticleFlags {
    pub user_id: Id,
    pub topic_ids: Vec<Id>,
    pub banned: bool,
    pub anonymous: bool,
    pub adult: bool,
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct CommentFlags {
    pub user_id: Id,
    pub banned: bool,
    pub anonymous: bool,
    pub adult: bool,
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct UserFlags {
    pub views: i64,
    pub reports_count: Vec<i64>,
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct ArticleStats {
    created_at: Time,
    views: i64,
    votes: i64,
    comments: i64,
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct CommentStats {
    article_id: Id,
    parent_comment_id: Option<Id>,
    created_at: Time,
    votes: i64,
    views: i64,
    comments: i64,
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct ArticleContent {
    title: String,
    html_content: String,
    format_id: Id,
    format_content: String,
}

#[derive(Clone, Hash, PartialEq, Eq)]
pub struct CommentContent {
    html_content: String,
    format_id: Id,
    format_content: String,
}

pub struct Database {
    cache: Cache<CacheKey, CacheValue>,
    backend: CacheBackend,
}

impl Database {
    pub async fn new(pool: Pool<Postgres>, cache_capacity: usize) -> Self {
        Self {
            cache: Cache::new(cache_capacity),
            backend: CacheBackend::new(pool),
        }
    }

    pub fn print_cache_type_sizes() {
        println!(
            "Cache key size: {}, Cache value size: {}",
            std::mem::size_of::<CacheKey>(),
            std::mem::size_of::<CacheValue>()
        )
    }

    db_getter!(UserReportThresholds, Vec<(Id, f64)>);
    db_getter!(UserViewedArticles, Option<Arcrw<HashSet<Id>>>);
    db_getter!(UserViewedComments, Option<Arcrw<HashSet<Id>>>);

    db_getter!(UserHiddenTopics, Option<Arcrw<HashSet<Id>>>);
    db_getter!(UserHiddenUsers, Option<Arcrw<HashSet<Id>>>);
    db_getter!(UserHiddenArticles, Option<Arcrw<HashSet<Id>>>);
    db_getter!(UserHiddenComments, Option<Arcrw<HashSet<Id>>>);

    db_getter!(ArticleFlags, ArticleFlags);
    db_getter!(CommentFlags, CommentFlags);
    db_getter!(UserFlags, UserFlags);

    db_getter!(CommentTimeRanker, Arcrw<TreeRanker<()>>);
    db_getter!(CommentViewsRanker, Arcrw<TreeRanker<i64>>);
    db_getter!(CommentVotesRanker, Arcrw<TreeRanker<i64>>);
    db_getter!(CommentCommentsRanker, Arcrw<TreeRanker<i64>>);
    db_getter!(CommentVotesPerViewsRanker, Arcrw<TreeRanker<NotNan<f64>>>);
    db_getter!(
        CommentCommentsPerViewsRanker,
        Arcrw<TreeRanker<NotNan<f64>>>
    );

    db_getter!(TopicArticleTimeRanker, Arcrw<SublistTimeRanker>);
    db_getter!(TopicArticleViewsRanker, Arcrw<SublistScoreRanker<i64>>);
    db_getter!(TopicArticleVotesRanker, Arcrw<SublistScoreRanker<i64>>);
    db_getter!(TopicArticleCommentsRanker, Arcrw<SublistScoreRanker<i64>>);
    db_getter!(
        TopicArticleVotesPerViewsRanker,
        Arcrw<SublistScoreRanker<NotNan<f64>>>
    );
    db_getter!(
        TopicArticleCommentsPerViewsRanker,
        Arcrw<SublistScoreRanker<NotNan<f64>>>
    );

    db_getter!(AuthorArticleTimeRanker, Arcrw<SublistTimeRanker>);
    db_getter!(AuthorArticleViewsRanker, Arcrw<SublistScoreRanker<i64>>);
    db_getter!(AuthorArticleVotesRanker, Arcrw<SublistScoreRanker<i64>>);
    db_getter!(AuthorArticleCommentsRanker, Arcrw<SublistScoreRanker<i64>>);
    db_getter!(
        AuthorArticleVotesPerViewsRanker,
        Arcrw<SublistScoreRanker<NotNan<f64>>>
    );
    db_getter!(
        AuthorArticleCommentsPerViewsRanker,
        Arcrw<SublistScoreRanker<NotNan<f64>>>
    );

    db_getter!(TopicArticleTimes, Arcrw<SublistRankerTimes>);
    db_getter!(AuthorArticleTimes, Arcrw<SublistRankerTimes>);
}
