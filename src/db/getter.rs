use super::{ArticleFlags, CommentFlags, Database, UserFlags};
use crate::{
    rank::{
        sublist::{SublistRankerTimes, SublistScoreRanker, SublistTimeRanker},
        tree::TreeRanker,
    },
    Arcrw, Id,
};
use ordered_float::NotNan;
use paste::paste;
use sqlx::Error;
use std::{collections::HashSet, sync::Arc};

pub trait DatabaseGetTreeRanker<S: Sync + Send + Copy + Ord> {
    async fn get(&self, id: Id) -> Result<Arcrw<TreeRanker<S>>, Error>;
}

pub trait DatabaseGetSublistTimeRanker {
    async fn get(&self, id: Id) -> Result<Arcrw<SublistTimeRanker>, Error>;
}

pub trait DatabaseGetSublistScoreRanker<S: Sync + Send + Copy + Ord> {
    async fn get(&self, id: Id) -> Result<Arcrw<SublistScoreRanker<S>>, Error>;
}

pub trait DatabaseGetSublistRankerTimes {
    async fn get(&self, id: Id) -> Result<Arcrw<SublistRankerTimes>, Error>;
}

pub trait DatabaseGetIdSet {
    async fn get(&self, id: Id) -> Result<Option<Arcrw<HashSet<Id>>>, Error>;
}

macro_rules! db_get_struct {
    ($get_function:ident, $ReturnType:ty) => {
        paste! {
            db_get_struct!($get_function, [<Database $get_function:camel>], $ReturnType);
        }
    };

    ($get_function:ident, $GetStruct:ident, $ReturnType:ty) => {
        #[derive(Clone)]
        pub struct $GetStruct {
            db: Arc<Database>,
        }

        impl $GetStruct {
            pub fn new(db: Arc<Database>) -> $GetStruct {
                $GetStruct { db }
            }
        }

        impl $GetStruct {
            pub async fn get(&self, id: Id) -> Result<$ReturnType, Error> {
                self.db.$get_function(id).await
            }
        }
    };
}

macro_rules! db_get_trait_struct {
    ($get_function:ident, $ReturnType:ty, $GetTrait:ty) => {
        paste! {
            db_get_trait_struct!($get_function, [<Database $get_function:camel>], $ReturnType, $GetTrait);
        }
    };

    ($get_function:ident, $GetStruct:ident, $ReturnType:ty, $GetTrait:ty) => {
        #[derive(Clone)]
        pub struct $GetStruct {
            db: Arc<Database>,
        }

        impl $GetStruct {
            pub fn new(db: Arc<Database>) -> $GetStruct {
                $GetStruct { db }
            }
        }

        impl $GetTrait for $GetStruct {
            async fn get(&self, id: Id) -> Result<$ReturnType, Error> {
                self.db.$get_function(id).await
            }
        }
    };
}

db_get_struct!(get_user_report_thresholds, Vec<(Id, f64)>);
db_get_trait_struct!(
    get_user_viewed_articles,
    Option<Arcrw<HashSet<Id>>>,
    DatabaseGetIdSet
);
db_get_trait_struct!(
    get_user_viewed_comments,
    Option<Arcrw<HashSet<Id>>>,
    DatabaseGetIdSet
);

db_get_trait_struct!(
    get_user_hidden_topics,
    Option<Arcrw<HashSet<Id>>>,
    DatabaseGetIdSet
);
db_get_trait_struct!(
    get_user_hidden_users,
    Option<Arcrw<HashSet<Id>>>,
    DatabaseGetIdSet
);
db_get_trait_struct!(
    get_user_hidden_articles,
    Option<Arcrw<HashSet<Id>>>,
    DatabaseGetIdSet
);
db_get_trait_struct!(
    get_user_hidden_comments,
    Option<Arcrw<HashSet<Id>>>,
    DatabaseGetIdSet
);

db_get_struct!(get_article_flags, ArticleFlags);
db_get_struct!(get_comment_flags, CommentFlags);
db_get_struct!(get_user_flags, UserFlags);

db_get_trait_struct!(
    get_comment_time_ranker,
    Arcrw<TreeRanker<()>>,
    DatabaseGetTreeRanker<()>
);
db_get_trait_struct!(
    get_comment_views_ranker,
    Arcrw<TreeRanker<i64>>,
    DatabaseGetTreeRanker<i64>
);
db_get_trait_struct!(
    get_comment_votes_ranker,
    Arcrw<TreeRanker<i64>>,
    DatabaseGetTreeRanker<i64>
);
db_get_trait_struct!(
    get_comment_comments_ranker,
    Arcrw<TreeRanker<i64>>,
    DatabaseGetTreeRanker<i64>
);
db_get_trait_struct!(
    get_comment_votes_per_views_ranker,
    Arcrw<TreeRanker<NotNan<f64>>>,
    DatabaseGetTreeRanker<NotNan<f64>>
);
db_get_trait_struct!(
    get_comment_comments_per_views_ranker,
    Arcrw<TreeRanker<NotNan<f64>>>,
    DatabaseGetTreeRanker<NotNan<f64>>
);

db_get_trait_struct!(
    get_topic_article_time_ranker,
    Arcrw<SublistTimeRanker>,
    DatabaseGetSublistTimeRanker
);
db_get_trait_struct!(
    get_topic_article_views_ranker,
    Arcrw<SublistScoreRanker<i64>>,
    DatabaseGetSublistScoreRanker<i64>
);
db_get_trait_struct!(
    get_topic_article_votes_ranker,
    Arcrw<SublistScoreRanker<i64>>,
    DatabaseGetSublistScoreRanker<i64>
);
db_get_trait_struct!(
    get_topic_article_comments_ranker,
    Arcrw<SublistScoreRanker<i64>>,
    DatabaseGetSublistScoreRanker<i64>
);
db_get_trait_struct!(
    get_topic_article_votes_per_views_ranker,
    Arcrw<SublistScoreRanker<NotNan<f64>>>,
    DatabaseGetSublistScoreRanker<NotNan<f64>>
);
db_get_trait_struct!(
    get_topic_article_comments_per_views_ranker,
    Arcrw<SublistScoreRanker<NotNan<f64>>>,
    DatabaseGetSublistScoreRanker<NotNan<f64>>
);

db_get_trait_struct!(
    get_author_article_time_ranker,
    Arcrw<SublistTimeRanker>,
    DatabaseGetSublistTimeRanker
);
db_get_trait_struct!(
    get_author_article_views_ranker,
    Arcrw<SublistScoreRanker<i64>>,
    DatabaseGetSublistScoreRanker<i64>
);
db_get_trait_struct!(
    get_author_article_votes_ranker,
    Arcrw<SublistScoreRanker<i64>>,
    DatabaseGetSublistScoreRanker<i64>
);
db_get_trait_struct!(
    get_author_article_comments_ranker,
    Arcrw<SublistScoreRanker<i64>>,
    DatabaseGetSublistScoreRanker<i64>
);
db_get_trait_struct!(
    get_author_article_votes_per_views_ranker,
    Arcrw<SublistScoreRanker<NotNan<f64>>>,
    DatabaseGetSublistScoreRanker<NotNan<f64>>
);
db_get_trait_struct!(
    get_author_article_comments_per_views_ranker,
    Arcrw<SublistScoreRanker<NotNan<f64>>>,
    DatabaseGetSublistScoreRanker<NotNan<f64>>
);

db_get_trait_struct!(
    get_topic_article_times,
    Arcrw<SublistRankerTimes>,
    DatabaseGetSublistRankerTimes
);
db_get_trait_struct!(
    get_author_article_times,
    Arcrw<SublistRankerTimes>,
    DatabaseGetSublistRankerTimes
);
