use super::{ArticleFlags, CommentFlags, UserFlags};
use crate::{
    rank::{
        sublist::{SublistRankerTimes, SublistScoreRanker, SublistTimeRanker},
        tree::TreeRanker,
    },
    Arcrw, Id,
};
use ordered_float::NotNan;
use sqlx::PgPool;
use std::{collections::HashSet, mem::size_of, sync::Arc};

#[derive(Clone, Hash, PartialEq, Eq)]
pub enum CacheKey {
    UserReportThresholds(Id),
    UserViewedArticles(Id),
    UserViewedComments(Id),

    UserHiddenTopics(Id),
    UserHiddenUsers(Id),
    UserHiddenArticles(Id),
    UserHiddenComments(Id),

    ArticleFlags(Id),
    CommentFlags(Id),
    UserFlags(Id),

    CommentTimeRanker(Id),
    CommentViewsRanker(Id),
    CommentVotesRanker(Id),
    CommentCommentsRanker(Id),
    CommentVotesPerViewsRanker(Id),
    CommentCommentsPerViewsRanker(Id),

    TopicArticleTimeRanker(Id),
    TopicArticleViewsRanker(Id),
    TopicArticleVotesRanker(Id),
    TopicArticleCommentsRanker(Id),
    TopicArticleVotesPerViewsRanker(Id),
    TopicArticleCommentsPerViewsRanker(Id),

    AuthorArticleTimeRanker(Id),
    AuthorArticleViewsRanker(Id),
    AuthorArticleVotesRanker(Id),
    AuthorArticleCommentsRanker(Id),
    AuthorArticleVotesPerViewsRanker(Id),
    AuthorArticleCommentsPerViewsRanker(Id),

    TopicArticleTimes(Id),
    AuthorArticleTimes(Id),
}

#[derive(Clone)]
pub enum CacheValue {
    UserReportThresholds(Vec<(Id, f64)>),
    UserViewedArticles(Option<Arcrw<HashSet<Id>>>),
    UserViewedComments(Option<Arcrw<HashSet<Id>>>),

    UserHiddenTopics(Option<Arcrw<HashSet<Id>>>),
    UserHiddenUsers(Option<Arcrw<HashSet<Id>>>),
    UserHiddenArticles(Option<Arcrw<HashSet<Id>>>),
    UserHiddenComments(Option<Arcrw<HashSet<Id>>>),

    ArticleFlags(ArticleFlags),
    CommentFlags(CommentFlags),
    UserFlags(UserFlags),

    CommentTimeRanker(Arcrw<TreeRanker<()>>),
    CommentViewsRanker(Arcrw<TreeRanker<i64>>),
    CommentVotesRanker(Arcrw<TreeRanker<i64>>),
    CommentCommentsRanker(Arcrw<TreeRanker<i64>>),
    CommentVotesPerViewsRanker(Arcrw<TreeRanker<NotNan<f64>>>),
    CommentCommentsPerViewsRanker(Arcrw<TreeRanker<NotNan<f64>>>),

    TopicArticleTimeRanker(Arcrw<SublistTimeRanker>),
    TopicArticleViewsRanker(Arcrw<SublistScoreRanker<i64>>),
    TopicArticleVotesRanker(Arcrw<SublistScoreRanker<i64>>),
    TopicArticleCommentsRanker(Arcrw<SublistScoreRanker<i64>>),
    TopicArticleVotesPerViewsRanker(Arcrw<SublistScoreRanker<NotNan<f64>>>),
    TopicArticleCommentsPerViewsRanker(Arcrw<SublistScoreRanker<NotNan<f64>>>),

    AuthorArticleTimeRanker(Arcrw<SublistTimeRanker>),
    AuthorArticleViewsRanker(Arcrw<SublistScoreRanker<i64>>),
    AuthorArticleVotesRanker(Arcrw<SublistScoreRanker<i64>>),
    AuthorArticleCommentsRanker(Arcrw<SublistScoreRanker<i64>>),
    AuthorArticleVotesPerViewsRanker(Arcrw<SublistScoreRanker<NotNan<f64>>>),
    AuthorArticleCommentsPerViewsRanker(Arcrw<SublistScoreRanker<NotNan<f64>>>),

    TopicArticleTimes(Arcrw<SublistRankerTimes>),
    AuthorArticleTimes(Arcrw<SublistRankerTimes>),
}

pub fn print_enum_sizes() {
    println!("Cachekey: {}", size_of::<CacheKey>());
    println!("CacheValue: {}", size_of::<CacheValue>());

    println!("UserReportThresholds: {}", size_of::<Vec<(Id, f64)>>());
    println!("UserHiddenTopics: {}", size_of::<Arcrw<HashSet<Id>>>());
    println!("UserHiddenUsers: {}", size_of::<Arcrw<HashSet<Id>>>());
    println!("UserHiddenArticles: {}", size_of::<Arcrw<HashSet<Id>>>());
    println!("UserHiddenComments: {}", size_of::<Arcrw<HashSet<Id>>>());

    println!("ArticleFlags: {}", size_of::<ArticleFlags>());
    println!("CommentFlags: {}", size_of::<CommentFlags>());
    println!("UserFlags: {}", size_of::<UserFlags>());

    println!("CommentTimeRanker: {}", size_of::<Arcrw<TreeRanker<()>>>());
    println!(
        "CommentViewsRanker: {}",
        size_of::<Arcrw<TreeRanker<i64>>>()
    );
    println!(
        "CommentVotesRanker: {}",
        size_of::<Arcrw<TreeRanker<i64>>>()
    );
    println!(
        "CommentCommentsRanker: {}",
        size_of::<Arcrw<TreeRanker<i64>>>()
    );
    println!(
        "CommentVotesPerViewsRanker: {}",
        size_of::<Arcrw<TreeRanker<NotNan<f64>>>>()
    );
    println!(
        "CommentCommentsPerViewsRanker: {}",
        size_of::<Arcrw<TreeRanker<NotNan<f64>>>>()
    );

    println!(
        "TopicArticleTimeRanker: {}",
        size_of::<Arcrw<SublistTimeRanker>>()
    );
    println!(
        "TopicArticleViewsRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<i64>>>()
    );
    println!(
        "TopicArticleVotesRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<i64>>>()
    );
    println!(
        "TopicArticleCommentsRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<i64>>>()
    );
    println!(
        "TopicArticleVotesPerViewsRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<NotNan<f64>>>>()
    );
    println!(
        "TopicArticleCommentsPerViewsRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<NotNan<f64>>>>()
    );

    println!(
        "AuthorArticleTimeRanker: {}",
        size_of::<Arcrw<SublistTimeRanker>>()
    );
    println!(
        "AuthorArticleViewsRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<i64>>>()
    );
    println!(
        "AuthorArticleVotesRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<i64>>>()
    );
    println!(
        "AuthorArticleCommentsRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<i64>>>()
    );
    println!(
        "AuthorArticleVotesPerViewsRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<NotNan<f64>>>>()
    );
    println!(
        "AuthorArticleCommentsPerViewsRanker: {}",
        size_of::<Arcrw<SublistScoreRanker<NotNan<f64>>>>()
    );

    println!(
        "TopicArticleTimes: {}",
        size_of::<Arcrw<SublistRankerTimes>>()
    );
    println!(
        "AuthorArticleTimes: {}",
        size_of::<Arcrw<SublistRankerTimes>>()
    );
}

#[derive(Debug, Clone)]
pub struct CacheBackend {
    pool: PgPool,
}

impl CacheBackend {
    pub fn new(pool: PgPool) -> Self {
        CacheBackend { pool }
    }

    pub async fn get_user_report_thresholds(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_user_viewed_articles(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_user_viewed_comments(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }

    pub async fn get_user_hidden_topics(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_user_hidden_users(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_user_hidden_articles(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_user_hidden_comments(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }

    pub async fn get_user_flags(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_article_flags(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_comment_flags(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }

    pub async fn get_comment_time_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_comment_views_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_comment_votes_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_comment_comments_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_comment_votes_per_views_ranker(
        &self,
        id: Id,
    ) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_comment_comments_per_views_ranker(
        &self,
        id: Id,
    ) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }

    pub async fn get_topic_article_time_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_topic_article_views_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_topic_article_votes_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_topic_article_comments_ranker(
        &self,
        id: Id,
    ) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_topic_article_votes_per_views_ranker(
        &self,
        id: Id,
    ) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_topic_article_comments_per_views_ranker(
        &self,
        id: Id,
    ) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }

    pub async fn get_author_article_time_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_author_article_views_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_author_article_votes_ranker(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_author_article_comments_ranker(
        &self,
        id: Id,
    ) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_author_article_votes_per_views_ranker(
        &self,
        id: Id,
    ) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_author_article_comments_per_views_ranker(
        &self,
        id: Id,
    ) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }

    pub async fn get_topic_article_times(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
    pub async fn get_author_article_times(&self, id: Id) -> Result<CacheValue, sqlx::Error> {
        todo!()
    }
}
