use self::{
    index::get_index,
    topics::{get_topics, get_topics_name},
};
use crate::{db::login::LoginBackend, state::AppState};
use axum::{http::StatusCode, routing::get, Router};
use axum_login::{tower_sessions::MemoryStore, AuthManagerLayer};
use std::sync::Arc;

mod index;
mod topics;

fn internal_error<E>(err: E) -> (StatusCode, String)
where
    E: std::error::Error,
{
    (StatusCode::INTERNAL_SERVER_ERROR, err.to_string())
}

pub fn build_router(
    shared_state: Arc<AppState>,
    auth_layer: AuthManagerLayer<LoginBackend, MemoryStore>,
) -> Router {
    Router::new()
        .route("/", get(get_index))
        .route("/topics", get(get_topics))
        .route("/topics/:topic_name", get(get_topics_name))
        //.route("/articles", get(todo!()).post(todo!()))
        //.route("/articles/:article_id", get(todo!()).post(todo!()))
        //.route("/articles/:article_id/comments", get(todo!()).post(todo!()))
        //.route(
        //    "/articles/:article_id/comments/:comment_id",
        //    get(todo!()).post(todo!()),
        //)
        //.route("/comments/:comment_id", get(todo!()).post(todo!()))
        //.route("/users/:user_id", get(todo!()).post(todo!()))
        //.route("/users/:user_id/articles", get(todo!()).post(todo!()))
        //.route("/users/:user_id/comments", get(todo!()).post(todo!()))
        //.route("/my", get(todo!()).post(todo!()))
        //.route("/my/topics", get(todo!()).post(todo!()))
        //.route("/my/articles", get(todo!()).post(todo!()))
        //.route("/my/comments", get(todo!()).post(todo!()))
        //.route("/my/filters", get(todo!()).post(todo!()))
        //.route("/login", get(todo!()).post(todo!()))
        .layer(auth_layer)
        .with_state(shared_state)
}
