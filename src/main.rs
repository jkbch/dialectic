use axum_login::{
    tower_sessions::{MemoryStore, SessionManagerLayer},
    AuthManagerLayerBuilder,
};
use chrono::{DateTime, Utc};
use db::{login::LoginBackend, Database};
use dotenvy::dotenv;
use parking_lot::RwLock;
use sqlx::postgres::PgPoolOptions;
use state::AppState;
use std::{collections::HashSet, env::var, sync::Arc};
use tracing_subscriber::{layer::SubscriberExt, util::SubscriberInitExt, EnvFilter};
use web::build_router;

mod db;
mod filter;
mod rank;
mod state;
mod web;

pub type Id = i64;
pub type Time = DateTime<Utc>;
type Arcrw<T> = Arc<RwLock<T>>;

fn lower_bound_wilson_95ci(s: i64, n: i64) -> f64 {
    let n = n as f64;
    let s = s as f64;
    let f = n - s;
    let z = 1.96;

    (s + z * z / 2.0) / (n + z * z) - z * (s * f / n + z * z / 4.0).sqrt() / (n + z * z)
}

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    dotenv().ok();

    tracing_subscriber::registry()
        .with(EnvFilter::new(var("RUST_LOG").unwrap()))
        .with(tracing_subscriber::fmt::layer())
        .try_init()?;

    let pool = PgPoolOptions::new()
        .connect(&var("DATABASE_URL").unwrap())
        .await
        .unwrap();

    let cache_capacity = 1000000;
    let db = Arc::new(Database::new(pool.clone(), cache_capacity).await);
    let cutoff = 1000;

    let default_report_thresholds = None;
    let default_hidden_topics = None;
    let default_hidden_users = None;
    let default_hidden_articles = None;
    let default_hidden_comments = None;

    let default_filter_anonymous = false;
    let default_filter_adult = true;
    let default_filter_viewed = false;

    let shared_state = Arc::new(AppState::new(
        db,
        cutoff,
        default_report_thresholds,
        default_hidden_topics,
        default_hidden_users,
        default_hidden_articles,
        default_hidden_comments,
        default_filter_anonymous,
        default_filter_adult,
        default_filter_viewed,
    ));

    let session_store = MemoryStore::default();
    let session_layer = SessionManagerLayer::new(session_store);
    let auth_layer = AuthManagerLayerBuilder::new(LoginBackend::new(pool), session_layer).build();

    let app = build_router(shared_state, auth_layer);
    let listener = tokio::net::TcpListener::bind(&var("WEBSITE_URL").unwrap())
        .await
        .unwrap();

    println!("listening on {}", listener.local_addr().unwrap());
    axum::serve(listener, app).await.unwrap();

    Ok(())
}
