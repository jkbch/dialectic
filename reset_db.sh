#!/bin/bash

read -r -p "Delete all data and reset the database? [y/N] "

if [[ $REPLY =~ ^[Yy]$ ]]
then
  dropdb debadia && createdb debadia && psql -d debadia -f schema.sql
fi

