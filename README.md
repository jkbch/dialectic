# Dialectic
A free/libre (as in freedom), open source Reddit-like website with a focus on
discussion. The site tries to fix some of the problems with Reddit like the
echo chamber effect and the overreaching of moderator power.

## Topic based system
The site categories posts by topics instead of subreddits. A post can have
multiple topics and topic is associated with a Wikipedia page defining the
topic. 

Some topic inherit other (parent) topics like the topic "socialism"
inheriting the topic "Left-wing politics" which inherits "Politics" and so
on. The topic hierarchy will be described as a polytree graph and will be navigable
on the website.

## User moderation
The sorting of the post is based on user votes and the time. There will be 3 types of 
sorting: most votes, lower bound of the 95% confidence interval of vote to view ratio, 
and newest time. It will not be possible to downvote to avoid a echo chamber.

The user will be able to filter post based on a given time period. Every post can be
reported for some kind of unwanted behavior and the user will be able to filter away
posts by users who have a history of unwanted behavior. The user can block
topics or users to filter away posts about a topic or from a user.

## Post formatting
Every post will be in markdown with inline images and videos, latex math and
code block with syntax highlighting for the most popular programming languages.
This ensures that the site is primarily focused on active discussion and not
meme content.

You will be able to link to an article, image or video inside the post
and the content will be fetched from the external website and shown it
inside the post.

## No advertisements
The site will never contain any kind of advertisement and never share you data
with third parties. When a website has advertisement the users attention
becomes the product and the website is incentivized to make the website as
addicting and time consuming as possible to maximize profit at the expense of
the users.

This site will be crowd-funded through donations. There will be a 'gold' system
like Reddit gold. 100 gold will be worth 1 EUR and they will be a 50%
transaction fee where half of the coins rounded up needs to be donated to a
charity every time someone give gold to another users post.

## Technology
The backend server will be a Postgres database and a HTTP REST API written in Rust
using Axum. The web app will be a Typescript Svelte app with Tailwind and 
will use Tauri for platform releases.

Post will be compiled from Markdown to HTML with Markedjs and purified with
DOMPurify. MathJax will be used to diplay Latex math and Graphviz will be used
to create the polytree topic graph in SVG.

## Algorithms and data structures
TODO

## HTTP REST API
TODO

## Postgres database
TODO

## Milestones
- [x] Git repository and basic description of project
- [ ] Algorithms and data structures description
- [ ] HTTP REST API design description
- [ ] Postgres database design description
- [ ] Backend server critical functionality complete
- [ ] Web app critical functionality complete
- [ ] Platform releases through Tauri
- [ ] Donation and gold system






